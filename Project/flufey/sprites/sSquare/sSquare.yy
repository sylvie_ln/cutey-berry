{
    "id": "ee8a8025-e516-41e7-811b-ee54a367e8c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe53290c-558c-401c-a17e-706a48ee8618",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee8a8025-e516-41e7-811b-ee54a367e8c9",
            "compositeImage": {
                "id": "84127c60-e18f-4704-91bd-da9d3127efac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe53290c-558c-401c-a17e-706a48ee8618",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a97034ea-4c49-4f36-9af1-67a5ed64e16c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe53290c-558c-401c-a17e-706a48ee8618",
                    "LayerId": "61979b0e-3190-4bbe-8a66-a41d5ba0e4b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "61979b0e-3190-4bbe-8a66-a41d5ba0e4b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee8a8025-e516-41e7-811b-ee54a367e8c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}