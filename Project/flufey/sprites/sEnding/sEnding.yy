{
    "id": "53bf9945-bae9-41a7-9eb6-37ecc098c224",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnding",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 1,
    "bbox_right": 315,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fc2798e-bce9-4ea9-bc95-c1d8491cda18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53bf9945-bae9-41a7-9eb6-37ecc098c224",
            "compositeImage": {
                "id": "23199bcb-465b-430f-8ec0-948f39d86e12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fc2798e-bce9-4ea9-bc95-c1d8491cda18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1dec4c-a44a-4aa4-95cd-d4b16897072a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fc2798e-bce9-4ea9-bc95-c1d8491cda18",
                    "LayerId": "be5cb81f-5928-45e5-afa4-15eb4633ebda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "be5cb81f-5928-45e5-afa4-15eb4633ebda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53bf9945-bae9-41a7-9eb6-37ecc098c224",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}