{
    "id": "21541ac3-e4cd-4d0e-b1fc-3a985cae7df6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDots02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd7e194b-aba1-43c4-b7e3-d01ff6553a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21541ac3-e4cd-4d0e-b1fc-3a985cae7df6",
            "compositeImage": {
                "id": "0644cb47-f980-40f8-bdeb-d66003ac908f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd7e194b-aba1-43c4-b7e3-d01ff6553a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68b8e0a6-d6d3-4b9a-873c-8c8c50758b7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd7e194b-aba1-43c4-b7e3-d01ff6553a51",
                    "LayerId": "74ca1799-2694-4ba4-a3cb-d6428ebb70b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "74ca1799-2694-4ba4-a3cb-d6428ebb70b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21541ac3-e4cd-4d0e-b1fc-3a985cae7df6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}