{
    "id": "d86e6074-2950-4aa1-8626-5d9fc354e804",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharFall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbc5363c-19f3-4c03-b2f5-ba32a52b4f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d86e6074-2950-4aa1-8626-5d9fc354e804",
            "compositeImage": {
                "id": "3d227be1-6c86-4504-9fd4-575ed92dec8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbc5363c-19f3-4c03-b2f5-ba32a52b4f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "475559e1-e789-4741-b25b-44fd3dc339ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbc5363c-19f3-4c03-b2f5-ba32a52b4f66",
                    "LayerId": "955f1744-02d4-4519-9c9b-0966bc9961f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "955f1744-02d4-4519-9c9b-0966bc9961f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d86e6074-2950-4aa1-8626-5d9fc354e804",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}