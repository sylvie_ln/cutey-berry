{
    "id": "2ccabe47-f1da-4ce6-a10a-aa69fd8dfb65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharCarryFall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9fec07d-d75e-499c-8951-dcef79196a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ccabe47-f1da-4ce6-a10a-aa69fd8dfb65",
            "compositeImage": {
                "id": "02ce92f3-6e06-4e29-bb3a-da9561b2aa3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9fec07d-d75e-499c-8951-dcef79196a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96367f0b-b525-430e-90e9-c120b9776f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9fec07d-d75e-499c-8951-dcef79196a11",
                    "LayerId": "331a450b-964e-4eb0-985d-76d177ff6d53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "331a450b-964e-4eb0-985d-76d177ff6d53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ccabe47-f1da-4ce6-a10a-aa69fd8dfb65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}