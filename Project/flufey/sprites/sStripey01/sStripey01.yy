{
    "id": "e7da4040-19fc-47d2-93e8-05f311e6ff2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStripey01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86400d28-76de-4838-bb2b-7cab4867d804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7da4040-19fc-47d2-93e8-05f311e6ff2c",
            "compositeImage": {
                "id": "41419c87-5bc2-4fe6-bc73-8711fcc2b39b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86400d28-76de-4838-bb2b-7cab4867d804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9a14945-ff9f-4f2d-bd2f-fec2ad37b14d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86400d28-76de-4838-bb2b-7cab4867d804",
                    "LayerId": "b55229ed-798c-4624-8ede-fff8d49da8a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b55229ed-798c-4624-8ede-fff8d49da8a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7da4040-19fc-47d2-93e8-05f311e6ff2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}