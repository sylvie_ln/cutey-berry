{
    "id": "381f7345-d77c-4b8b-b26a-d8e9cf5fb13f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKitteyHighlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c13903c-0360-4011-8972-30457f7124b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "381f7345-d77c-4b8b-b26a-d8e9cf5fb13f",
            "compositeImage": {
                "id": "86da2da9-2920-4edb-bfb3-6e6cd621b555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c13903c-0360-4011-8972-30457f7124b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "042960e0-cc23-4dc3-81e2-8c281ee25065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c13903c-0360-4011-8972-30457f7124b5",
                    "LayerId": "20d7eee4-ef31-4127-af68-76f7d37dfdfe"
                }
            ]
        },
        {
            "id": "3997329b-6ce7-4402-a602-08ebcd1a6cba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "381f7345-d77c-4b8b-b26a-d8e9cf5fb13f",
            "compositeImage": {
                "id": "f6d671d8-7376-4aef-a780-c0b920ab70b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3997329b-6ce7-4402-a602-08ebcd1a6cba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a17407e-ba91-4318-bf79-d49957375003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3997329b-6ce7-4402-a602-08ebcd1a6cba",
                    "LayerId": "20d7eee4-ef31-4127-af68-76f7d37dfdfe"
                }
            ]
        },
        {
            "id": "7279b14d-225a-4d06-b148-089bcc266e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "381f7345-d77c-4b8b-b26a-d8e9cf5fb13f",
            "compositeImage": {
                "id": "00a06140-5a08-4d60-bffe-21527ecb3ff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7279b14d-225a-4d06-b148-089bcc266e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c30a160-9e32-4d29-a40a-5c0bebf74553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7279b14d-225a-4d06-b148-089bcc266e70",
                    "LayerId": "20d7eee4-ef31-4127-af68-76f7d37dfdfe"
                }
            ]
        },
        {
            "id": "84183358-edbb-4aa2-a561-064a0202fef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "381f7345-d77c-4b8b-b26a-d8e9cf5fb13f",
            "compositeImage": {
                "id": "837fc6da-e623-4c9a-a9e8-03a9ca8cdaed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84183358-edbb-4aa2-a561-064a0202fef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e2a736b-85b5-4270-a8d8-4f6373d4b467",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84183358-edbb-4aa2-a561-064a0202fef3",
                    "LayerId": "20d7eee4-ef31-4127-af68-76f7d37dfdfe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "20d7eee4-ef31-4127-af68-76f7d37dfdfe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "381f7345-d77c-4b8b-b26a-d8e9cf5fb13f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294437317,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}