{
    "id": "508b6b1b-4857-47ca-9623-7944f217340b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 153,
    "bbox_left": 40,
    "bbox_right": 285,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc96c863-57a6-40c3-acb3-6308dac4b7f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "508b6b1b-4857-47ca-9623-7944f217340b",
            "compositeImage": {
                "id": "355c2dca-cedf-43c3-9834-b472884d8570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc96c863-57a6-40c3-acb3-6308dac4b7f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f887e797-c123-48cb-8ca6-3c5a4e148baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc96c863-57a6-40c3-acb3-6308dac4b7f8",
                    "LayerId": "69f5f1b9-d8e6-41af-bbb3-4ff9aaca6d5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "69f5f1b9-d8e6-41af-bbb3-4ff9aaca6d5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "508b6b1b-4857-47ca-9623-7944f217340b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}