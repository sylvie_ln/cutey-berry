{
    "id": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sConfigFaceButtons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40d57aa4-cee9-46ef-bdd7-efcb3dbbf87e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "f24b770e-8aaf-40a0-8cc1-036a3a6ecdd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40d57aa4-cee9-46ef-bdd7-efcb3dbbf87e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f16caffa-1e4b-401d-9805-45f1d2cfef1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d57aa4-cee9-46ef-bdd7-efcb3dbbf87e",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "b2170618-ce0d-429a-b11b-b24a6b780808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d57aa4-cee9-46ef-bdd7-efcb3dbbf87e",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "6f7c2854-545e-4eae-8a63-0eb5d446e7e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "8a323dd2-4f78-4569-ae35-cd5b164424e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7c2854-545e-4eae-8a63-0eb5d446e7e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fd16a6b-eef3-4260-b3a7-ce5f460ae8ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7c2854-545e-4eae-8a63-0eb5d446e7e3",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "c53bb9a4-e4bd-4ed5-9486-8757654381db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7c2854-545e-4eae-8a63-0eb5d446e7e3",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "c25d65e6-7a80-4c81-9364-6e1153ba88d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "2c7b7657-36d3-4bdd-b33c-c80d7828e80a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c25d65e6-7a80-4c81-9364-6e1153ba88d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3ea7d9b-e23a-45e9-927b-95dc8a0baadb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c25d65e6-7a80-4c81-9364-6e1153ba88d7",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "7808c6b3-9be4-4a54-8931-0cbdeef3fa28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c25d65e6-7a80-4c81-9364-6e1153ba88d7",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "3c604c2d-4586-4353-9502-cbdf5bd6560d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "657d3fb0-c937-413f-b581-f24a033a37bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c604c2d-4586-4353-9502-cbdf5bd6560d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc7b06ba-09e4-4cfc-a921-35198f273798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c604c2d-4586-4353-9502-cbdf5bd6560d",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "2457f585-2df7-41f4-9fff-91aaf31ea575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c604c2d-4586-4353-9502-cbdf5bd6560d",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "8cdebc1d-8ce5-45a2-a497-68d356a03207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "0b16bdeb-9755-48bb-9a2f-5f89677844fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdebc1d-8ce5-45a2-a497-68d356a03207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01a2213f-9e51-439d-ad1c-066cd4b51a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdebc1d-8ce5-45a2-a497-68d356a03207",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "1a22a773-9a71-43a3-8c80-f4b0a635046e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdebc1d-8ce5-45a2-a497-68d356a03207",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "65b2a6fd-4d7c-40c4-a035-d54913b4445d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "3a58ba8f-8bae-4905-bd1a-55c13069826b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65b2a6fd-4d7c-40c4-a035-d54913b4445d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13cec73c-15fd-4d4a-92f9-91dfe9336ba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b2a6fd-4d7c-40c4-a035-d54913b4445d",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "985d5c2e-7663-49e4-86aa-0ed917897376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b2a6fd-4d7c-40c4-a035-d54913b4445d",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "e972e96d-abca-4a69-a723-b7de0d332018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "d43ddb7c-74fe-41c4-a9f7-cd188eb0cb18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e972e96d-abca-4a69-a723-b7de0d332018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b307b0b7-61b3-40b0-9627-85669c734c63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e972e96d-abca-4a69-a723-b7de0d332018",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "92c28a19-0ed7-400b-92bc-06b0eba38848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e972e96d-abca-4a69-a723-b7de0d332018",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "38832163-16de-4ef2-bdce-ed8cb3cd392c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "042dc2bf-9391-4d2d-8428-abb9069038e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38832163-16de-4ef2-bdce-ed8cb3cd392c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1ef48d4-7ba5-4e09-b172-85e2f3fec325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38832163-16de-4ef2-bdce-ed8cb3cd392c",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "7739bbcf-3343-4e4e-b583-ba954575560b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38832163-16de-4ef2-bdce-ed8cb3cd392c",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "f6a82e64-baa8-4899-8333-6951aeaf7eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "97f1cf5e-2887-4353-a33e-b124f21fcccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6a82e64-baa8-4899-8333-6951aeaf7eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b5d72b7-68c7-4263-ad78-812b86a5e68a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a82e64-baa8-4899-8333-6951aeaf7eba",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "11fc085c-0301-4a5c-988f-a913e00a3cf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a82e64-baa8-4899-8333-6951aeaf7eba",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "815d3da9-d9f1-4273-90c0-66703a338cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "f31fd68e-fb79-44ff-b979-0bcf86e5f427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815d3da9-d9f1-4273-90c0-66703a338cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7352c65e-e560-4b17-8739-5ad5e83b622c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815d3da9-d9f1-4273-90c0-66703a338cd6",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "c9049a0c-c9a6-4454-986f-5691bb51a7ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815d3da9-d9f1-4273-90c0-66703a338cd6",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "869abc09-27b6-43a9-aceb-8fa4ac05623d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "c3f9b5a3-f60c-448f-9d54-9a03844ed115",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "869abc09-27b6-43a9-aceb-8fa4ac05623d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c65f9c-8d59-419c-a50c-7dd48b3076b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "869abc09-27b6-43a9-aceb-8fa4ac05623d",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "877cbd1a-7170-402c-a3f8-babe94c24292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "869abc09-27b6-43a9-aceb-8fa4ac05623d",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "0046bfbf-6df8-4696-89fb-a7939341a03a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "749d958d-4d9d-494e-8acb-5fbcf3ac5934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0046bfbf-6df8-4696-89fb-a7939341a03a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30727db4-4f8e-4597-a7d4-bd4ba2deba22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0046bfbf-6df8-4696-89fb-a7939341a03a",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "d7530039-85ac-4e78-9333-8b50b7f2a91c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0046bfbf-6df8-4696-89fb-a7939341a03a",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "cedd722b-6ed5-40bf-8de0-cb2b952c13c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "4a1d5d1f-49f7-4054-ba4f-69ee4225c028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cedd722b-6ed5-40bf-8de0-cb2b952c13c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97e6ba01-f0f2-442b-8f56-70d5d43f9011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cedd722b-6ed5-40bf-8de0-cb2b952c13c0",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "729f905e-3d8b-4b42-9341-7084942a1cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cedd722b-6ed5-40bf-8de0-cb2b952c13c0",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "f7f668ae-3ef0-48a8-ad72-b7643600df1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "7723e575-9d3e-463c-af55-09368d5035b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f668ae-3ef0-48a8-ad72-b7643600df1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc111a9f-19bd-4dea-9d3c-e2d4cfce7ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f668ae-3ef0-48a8-ad72-b7643600df1b",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "f4ac086d-3115-4f8d-82bf-c58d21650dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f668ae-3ef0-48a8-ad72-b7643600df1b",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "5d84c7fb-598f-4ef9-bd33-80da51a78e76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "b6ee54d7-0900-4bf3-9f4a-6b456493423e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d84c7fb-598f-4ef9-bd33-80da51a78e76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f64bfcff-2d16-462a-b9e0-59b642a3c3c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d84c7fb-598f-4ef9-bd33-80da51a78e76",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "26995501-1b82-4c4c-a03f-22eaf210ba5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d84c7fb-598f-4ef9-bd33-80da51a78e76",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        },
        {
            "id": "a0487acd-fcdc-443f-963a-72011ff259bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "compositeImage": {
                "id": "bbb4f3f3-f568-4fb4-ba39-d413077d6702",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0487acd-fcdc-443f-963a-72011ff259bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45cc92a3-4eac-4e6c-9fc6-8dfb9bb1547d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0487acd-fcdc-443f-963a-72011ff259bb",
                    "LayerId": "5d948eab-9cb8-4232-bc5a-285ae03e76e7"
                },
                {
                    "id": "b88cd1b5-529d-4e25-af65-c54d4992800f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0487acd-fcdc-443f-963a-72011ff259bb",
                    "LayerId": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 16,
    "layers": [
        {
            "id": "5d948eab-9cb8-4232-bc5a-285ae03e76e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "444eaa54-fc15-4d3e-a9e5-1ad15fff4151",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a9c1bfc-f507-4aa8-a230-714d2dfa9a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}