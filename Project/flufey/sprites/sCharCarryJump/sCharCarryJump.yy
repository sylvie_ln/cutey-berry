{
    "id": "a5244cd0-ba44-44ec-95a5-9224f6029522",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharCarryJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6913203e-b414-4dcf-b192-7dcff20562be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5244cd0-ba44-44ec-95a5-9224f6029522",
            "compositeImage": {
                "id": "d5867a81-ef91-4ce3-8d21-037b5051d22f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6913203e-b414-4dcf-b192-7dcff20562be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b26fde0-5d55-4c09-a22a-047df035ccb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6913203e-b414-4dcf-b192-7dcff20562be",
                    "LayerId": "c3d9789c-de9a-495e-9305-626e6f2edd5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c3d9789c-de9a-495e-9305-626e6f2edd5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5244cd0-ba44-44ec-95a5-9224f6029522",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}