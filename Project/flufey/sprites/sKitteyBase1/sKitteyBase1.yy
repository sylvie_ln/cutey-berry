{
    "id": "5340c5d5-4959-4211-9aee-a6d33c446b77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKitteyBase1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d8d9dbc-26f8-4f1a-a368-2ddec772550c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5340c5d5-4959-4211-9aee-a6d33c446b77",
            "compositeImage": {
                "id": "e8be31fd-a01e-455e-b896-564d9ccf3c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d8d9dbc-26f8-4f1a-a368-2ddec772550c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0928281-6e03-48dc-b8e5-2fa048493520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d8d9dbc-26f8-4f1a-a368-2ddec772550c",
                    "LayerId": "5076d85e-058c-473b-86aa-fa9936e5e6f8"
                }
            ]
        },
        {
            "id": "cca702f4-3fb2-4841-806d-b0fdd54cc7f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5340c5d5-4959-4211-9aee-a6d33c446b77",
            "compositeImage": {
                "id": "2be7ca51-7819-46b1-9b11-304d1a1b4ce1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca702f4-3fb2-4841-806d-b0fdd54cc7f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f852edf-36c9-4143-a8d6-45175c77ed73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca702f4-3fb2-4841-806d-b0fdd54cc7f6",
                    "LayerId": "5076d85e-058c-473b-86aa-fa9936e5e6f8"
                }
            ]
        },
        {
            "id": "bb42326e-e6ab-44aa-b06a-e998bc546860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5340c5d5-4959-4211-9aee-a6d33c446b77",
            "compositeImage": {
                "id": "a387cb0b-e432-4ad7-b4dc-0150b94398e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb42326e-e6ab-44aa-b06a-e998bc546860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca982db-fcd1-4d6d-8d3c-b521b4f4cc05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb42326e-e6ab-44aa-b06a-e998bc546860",
                    "LayerId": "5076d85e-058c-473b-86aa-fa9936e5e6f8"
                }
            ]
        },
        {
            "id": "5928e193-6374-43ab-bdea-6c5c376e57a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5340c5d5-4959-4211-9aee-a6d33c446b77",
            "compositeImage": {
                "id": "b645f118-76b5-45b1-ad94-31105ad71c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5928e193-6374-43ab-bdea-6c5c376e57a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44933bf2-5ad3-424c-87c1-e50a507c1727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5928e193-6374-43ab-bdea-6c5c376e57a3",
                    "LayerId": "5076d85e-058c-473b-86aa-fa9936e5e6f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5076d85e-058c-473b-86aa-fa9936e5e6f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5340c5d5-4959-4211-9aee-a6d33c446b77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294437317,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}