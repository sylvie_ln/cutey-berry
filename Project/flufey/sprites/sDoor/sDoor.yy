{
    "id": "af38aac6-d55e-4331-bad5-293d64444338",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f2dba28-1634-43ce-8620-8954a2001e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af38aac6-d55e-4331-bad5-293d64444338",
            "compositeImage": {
                "id": "f7c56693-53f0-46fb-a14a-a53af8ea144e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2dba28-1634-43ce-8620-8954a2001e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0ae6352-ac1e-4191-86de-01b009738b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2dba28-1634-43ce-8620-8954a2001e9e",
                    "LayerId": "875b0c29-8f2f-4938-8e26-3647bc57fb6e"
                }
            ]
        },
        {
            "id": "32eac93d-6fb5-42e9-92e1-e4e5f34f5618",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af38aac6-d55e-4331-bad5-293d64444338",
            "compositeImage": {
                "id": "4ca5824f-a7af-4b8e-8a7c-1316b458c044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32eac93d-6fb5-42e9-92e1-e4e5f34f5618",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a97eb03-6d43-4d6e-8111-8ddd10e3b943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32eac93d-6fb5-42e9-92e1-e4e5f34f5618",
                    "LayerId": "875b0c29-8f2f-4938-8e26-3647bc57fb6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "875b0c29-8f2f-4938-8e26-3647bc57fb6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af38aac6-d55e-4331-bad5-293d64444338",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}