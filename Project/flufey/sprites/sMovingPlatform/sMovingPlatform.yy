{
    "id": "9bc4060d-895a-4c79-8c5f-d3f15edf0f69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMovingPlatform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a51a1eac-06d7-4cc3-b678-3d5cee03cd20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc4060d-895a-4c79-8c5f-d3f15edf0f69",
            "compositeImage": {
                "id": "4a229c97-b62e-4b59-943c-5e51d0cb8f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a51a1eac-06d7-4cc3-b678-3d5cee03cd20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b622dee-1c9a-4236-b619-ab6feed9660c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a51a1eac-06d7-4cc3-b678-3d5cee03cd20",
                    "LayerId": "d6723da0-e26e-40ed-a0b0-980a8ad997bb"
                }
            ]
        },
        {
            "id": "1d20333e-d3a7-47cc-abc8-e1df80a0a61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc4060d-895a-4c79-8c5f-d3f15edf0f69",
            "compositeImage": {
                "id": "9f35c850-a26d-4681-b19f-385826cac1ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d20333e-d3a7-47cc-abc8-e1df80a0a61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeabba8a-6c51-4591-a75e-43eb99767f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d20333e-d3a7-47cc-abc8-e1df80a0a61a",
                    "LayerId": "d6723da0-e26e-40ed-a0b0-980a8ad997bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d6723da0-e26e-40ed-a0b0-980a8ad997bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bc4060d-895a-4c79-8c5f-d3f15edf0f69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}