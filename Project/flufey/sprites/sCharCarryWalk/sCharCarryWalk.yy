{
    "id": "a0ff097f-88d9-4939-8a94-b897554004ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharCarryWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b672534-b9a8-48cb-a28b-21b479ef2472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ff097f-88d9-4939-8a94-b897554004ac",
            "compositeImage": {
                "id": "e76e9593-d95a-4d22-85a2-2cadd54d20be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b672534-b9a8-48cb-a28b-21b479ef2472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "769b82c0-4558-4303-9f48-6762645b0461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b672534-b9a8-48cb-a28b-21b479ef2472",
                    "LayerId": "054a02b4-9700-4978-8ae4-cec0db3f87a8"
                }
            ]
        },
        {
            "id": "ee6314d1-8126-4358-85b9-19a447c3d608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ff097f-88d9-4939-8a94-b897554004ac",
            "compositeImage": {
                "id": "57043bc6-5d53-4608-8474-3b73996a7482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee6314d1-8126-4358-85b9-19a447c3d608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadcfec0-89a0-47fd-989c-4114c7d4c538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee6314d1-8126-4358-85b9-19a447c3d608",
                    "LayerId": "054a02b4-9700-4978-8ae4-cec0db3f87a8"
                }
            ]
        },
        {
            "id": "3c233c4c-a3ae-4569-88a6-0df0b97cf463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ff097f-88d9-4939-8a94-b897554004ac",
            "compositeImage": {
                "id": "0b1e7711-b367-47f7-87d0-20abba3ffe42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c233c4c-a3ae-4569-88a6-0df0b97cf463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f267b2e0-7a97-4236-a6e1-5d701f3b1802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c233c4c-a3ae-4569-88a6-0df0b97cf463",
                    "LayerId": "054a02b4-9700-4978-8ae4-cec0db3f87a8"
                }
            ]
        },
        {
            "id": "8fea3477-b989-4749-8f71-e9b1dbdd658d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0ff097f-88d9-4939-8a94-b897554004ac",
            "compositeImage": {
                "id": "5bcdecda-a1ee-4ea3-b954-911b941e30c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fea3477-b989-4749-8f71-e9b1dbdd658d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed29cd76-18f2-409c-8fbc-dd4b8d322fb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fea3477-b989-4749-8f71-e9b1dbdd658d",
                    "LayerId": "054a02b4-9700-4978-8ae4-cec0db3f87a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "054a02b4-9700-4978-8ae4-cec0db3f87a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0ff097f-88d9-4939-8a94-b897554004ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}