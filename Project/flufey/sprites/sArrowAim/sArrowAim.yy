{
    "id": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sArrowAim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 5,
    "bbox_right": 12,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28450ce3-3863-4091-9e60-887a41696c20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "compositeImage": {
                "id": "9a9d6b75-4110-4ea8-8d89-fcbe6024b21d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28450ce3-3863-4091-9e60-887a41696c20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff32919e-f8ce-4f9a-b518-4c10181a5abb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28450ce3-3863-4091-9e60-887a41696c20",
                    "LayerId": "52d8978b-8b66-4b3b-9ffb-15508477038e"
                }
            ]
        },
        {
            "id": "8ba19ee8-aa1d-4182-a43a-4821edc08914",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "compositeImage": {
                "id": "0ae9b575-4689-4f7a-95f2-386ac3663949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba19ee8-aa1d-4182-a43a-4821edc08914",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3540912c-7e27-4c5a-be97-852b514a8b27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba19ee8-aa1d-4182-a43a-4821edc08914",
                    "LayerId": "52d8978b-8b66-4b3b-9ffb-15508477038e"
                }
            ]
        },
        {
            "id": "43f8dc13-9008-4e7c-8937-fd2093d65246",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "compositeImage": {
                "id": "84a25da0-5801-42d9-b262-79bc3cb7a860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f8dc13-9008-4e7c-8937-fd2093d65246",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feae8918-0aca-41ac-b888-93749f3b6164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f8dc13-9008-4e7c-8937-fd2093d65246",
                    "LayerId": "52d8978b-8b66-4b3b-9ffb-15508477038e"
                }
            ]
        },
        {
            "id": "f25a8656-f5f5-482b-acda-c8988f5e98ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "compositeImage": {
                "id": "7fb5a46d-dfe6-4b18-8640-b08a0d5794d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f25a8656-f5f5-482b-acda-c8988f5e98ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "720dfcf7-617d-402a-a106-fe3a2ea92f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f25a8656-f5f5-482b-acda-c8988f5e98ca",
                    "LayerId": "52d8978b-8b66-4b3b-9ffb-15508477038e"
                }
            ]
        },
        {
            "id": "02523b91-6f84-4177-89c1-7c539d87c4b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "compositeImage": {
                "id": "c5a70c41-4f7d-417c-a862-6236ff880a0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02523b91-6f84-4177-89c1-7c539d87c4b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6ded71-c397-4be2-af85-a1ffec75dab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02523b91-6f84-4177-89c1-7c539d87c4b8",
                    "LayerId": "52d8978b-8b66-4b3b-9ffb-15508477038e"
                }
            ]
        },
        {
            "id": "e774e8f1-7ab7-4c04-a58b-3ade28ff8d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "compositeImage": {
                "id": "87093003-f69e-4464-9565-70cdea1b4f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e774e8f1-7ab7-4c04-a58b-3ade28ff8d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb0046f-9026-4aaf-b9ea-91b2117dc87b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e774e8f1-7ab7-4c04-a58b-3ade28ff8d48",
                    "LayerId": "52d8978b-8b66-4b3b-9ffb-15508477038e"
                }
            ]
        },
        {
            "id": "a75e748e-3324-4141-9fb9-9fb18c1bcda1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "compositeImage": {
                "id": "f473145c-f17c-44e2-8847-973045fcc867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a75e748e-3324-4141-9fb9-9fb18c1bcda1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688deb06-c0f9-4f03-b0cf-09cd16d71d7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a75e748e-3324-4141-9fb9-9fb18c1bcda1",
                    "LayerId": "52d8978b-8b66-4b3b-9ffb-15508477038e"
                }
            ]
        },
        {
            "id": "5fa8ccf6-680b-4abc-aa13-faad2f659474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "compositeImage": {
                "id": "8a3fddbb-f9d4-47f3-9353-6657184940e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa8ccf6-680b-4abc-aa13-faad2f659474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74617908-31c6-49fe-b49c-4b1572d1ba66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa8ccf6-680b-4abc-aa13-faad2f659474",
                    "LayerId": "52d8978b-8b66-4b3b-9ffb-15508477038e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "52d8978b-8b66-4b3b-9ffb-15508477038e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6efc11fe-a245-4c7e-89db-afd1eb76b5ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}