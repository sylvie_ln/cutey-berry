{
    "id": "7a51b964-a608-4858-8ef7-6a4ecb29c1cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHoopWinbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2314a35d-0f36-4e61-9bfa-5e0b5594d946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a51b964-a608-4858-8ef7-6a4ecb29c1cc",
            "compositeImage": {
                "id": "d165de86-8aa9-4ba2-bb27-6c85422d86c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2314a35d-0f36-4e61-9bfa-5e0b5594d946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee18e797-6e34-44c8-8f3e-ecc78200103a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2314a35d-0f36-4e61-9bfa-5e0b5594d946",
                    "LayerId": "d93bb6b0-a81b-46d7-b943-89aedf507ffb"
                },
                {
                    "id": "e5363b67-e5e2-40b1-84f5-a937f2459298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2314a35d-0f36-4e61-9bfa-5e0b5594d946",
                    "LayerId": "0acfeacb-6bff-49b5-a00a-bd92a3027412"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d93bb6b0-a81b-46d7-b943-89aedf507ffb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a51b964-a608-4858-8ef7-6a4ecb29c1cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0acfeacb-6bff-49b5-a00a-bd92a3027412",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a51b964-a608-4858-8ef7-6a4ecb29c1cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}