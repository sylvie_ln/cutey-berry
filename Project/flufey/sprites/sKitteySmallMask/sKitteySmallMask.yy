{
    "id": "7aa0f00c-98ba-44f7-84e0-c8d047c71192",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKitteySmallMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e752a2f9-c970-4ca0-91bf-678d03138cf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7aa0f00c-98ba-44f7-84e0-c8d047c71192",
            "compositeImage": {
                "id": "8f36a0af-b9be-4b41-b7f6-c54438bfbd0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e752a2f9-c970-4ca0-91bf-678d03138cf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92aced96-6e4c-4446-b1b6-075fd0ebe9b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e752a2f9-c970-4ca0-91bf-678d03138cf9",
                    "LayerId": "1a5b7206-7688-4dd7-892f-9d7d062b523c"
                }
            ]
        },
        {
            "id": "8f5a6c4a-3150-4073-8fc4-91f825e5e954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7aa0f00c-98ba-44f7-84e0-c8d047c71192",
            "compositeImage": {
                "id": "a9759ce6-07a5-44cc-8637-035d074de891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f5a6c4a-3150-4073-8fc4-91f825e5e954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac9a4d5-230d-4a07-b5c3-26ef68184ba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f5a6c4a-3150-4073-8fc4-91f825e5e954",
                    "LayerId": "1a5b7206-7688-4dd7-892f-9d7d062b523c"
                }
            ]
        },
        {
            "id": "9ccd7069-4334-4e0a-912b-4c414148d724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7aa0f00c-98ba-44f7-84e0-c8d047c71192",
            "compositeImage": {
                "id": "32948af0-fab9-44d8-90f5-3e29aab3e4d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ccd7069-4334-4e0a-912b-4c414148d724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "156aa1e7-6b12-42bd-afe6-2039f1ac62ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ccd7069-4334-4e0a-912b-4c414148d724",
                    "LayerId": "1a5b7206-7688-4dd7-892f-9d7d062b523c"
                }
            ]
        },
        {
            "id": "dc60985a-b662-40df-bc47-fe413d78d4a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7aa0f00c-98ba-44f7-84e0-c8d047c71192",
            "compositeImage": {
                "id": "6daf0339-0337-4493-ac12-b116d069c226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc60985a-b662-40df-bc47-fe413d78d4a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "182c9d86-fcb9-48a8-ae27-b1e57bfa9e05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc60985a-b662-40df-bc47-fe413d78d4a2",
                    "LayerId": "1a5b7206-7688-4dd7-892f-9d7d062b523c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1a5b7206-7688-4dd7-892f-9d7d062b523c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7aa0f00c-98ba-44f7-84e0-c8d047c71192",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294437317,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}