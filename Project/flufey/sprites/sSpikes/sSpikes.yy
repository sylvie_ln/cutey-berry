{
    "id": "1b066efa-ab77-4dfe-9b3b-47dde7a8c3ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSpikes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c7642eb-4d22-478d-b827-86083f43bc85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b066efa-ab77-4dfe-9b3b-47dde7a8c3ba",
            "compositeImage": {
                "id": "0fc201d7-67c9-4321-ba57-6df4be823f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c7642eb-4d22-478d-b827-86083f43bc85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b753539f-b5b4-47b2-913b-07a177c38c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c7642eb-4d22-478d-b827-86083f43bc85",
                    "LayerId": "b7cc235a-ecc9-4e15-8d73-72a536aa9d74"
                }
            ]
        },
        {
            "id": "dbb88203-c6d6-4c7c-8e4e-93b4bbeaf4d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b066efa-ab77-4dfe-9b3b-47dde7a8c3ba",
            "compositeImage": {
                "id": "5eacbe6a-a1a1-43c6-a86a-d809adf9b395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbb88203-c6d6-4c7c-8e4e-93b4bbeaf4d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e3d5cf3-c170-4035-bfd8-4c3ac8f9cfa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb88203-c6d6-4c7c-8e4e-93b4bbeaf4d0",
                    "LayerId": "b7cc235a-ecc9-4e15-8d73-72a536aa9d74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b7cc235a-ecc9-4e15-8d73-72a536aa9d74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b066efa-ab77-4dfe-9b3b-47dde7a8c3ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}