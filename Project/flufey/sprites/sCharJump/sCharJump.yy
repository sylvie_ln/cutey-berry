{
    "id": "67e9f666-ead3-434d-a4f5-f8fd43ab051e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3592305-8557-4053-b0ae-e07a223b4b06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67e9f666-ead3-434d-a4f5-f8fd43ab051e",
            "compositeImage": {
                "id": "6b03370d-4706-440d-ac5b-3147920434f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3592305-8557-4053-b0ae-e07a223b4b06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09a0dab-eb66-4ae7-849f-42706937d272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3592305-8557-4053-b0ae-e07a223b4b06",
                    "LayerId": "e8349cc3-f5e2-4fd4-8d08-3cc4daff6cbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e8349cc3-f5e2-4fd4-8d08-3cc4daff6cbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67e9f666-ead3-434d-a4f5-f8fd43ab051e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}