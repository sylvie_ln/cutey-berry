{
    "id": "eb6b54c9-8df4-4dc1-8297-f1836cbf90e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharCarryStand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1d034ad-67a6-4697-b419-d5c4a7feb986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb6b54c9-8df4-4dc1-8297-f1836cbf90e4",
            "compositeImage": {
                "id": "c78fcacd-0887-4217-b5ed-99af58b3f42e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d034ad-67a6-4697-b419-d5c4a7feb986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1333a3f4-018e-4d6f-a581-d0084abdac09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d034ad-67a6-4697-b419-d5c4a7feb986",
                    "LayerId": "65a7e50b-24bb-4e2c-a259-538a1ada1c8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "65a7e50b-24bb-4e2c-a259-538a1ada1c8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb6b54c9-8df4-4dc1-8297-f1836cbf90e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}