{
    "id": "df5451ed-9cb5-4286-b2e1-02bbf15e0936",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSquareHit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b854f89-2b8d-4c42-bab8-c6a88901132f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5451ed-9cb5-4286-b2e1-02bbf15e0936",
            "compositeImage": {
                "id": "255e3f25-9184-4f2e-ad77-26f8a0247ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b854f89-2b8d-4c42-bab8-c6a88901132f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a783f309-8538-473f-8e24-be95df732aac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b854f89-2b8d-4c42-bab8-c6a88901132f",
                    "LayerId": "8b511603-59b2-4689-b870-6d8e649a3caf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8b511603-59b2-4689-b870-6d8e649a3caf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df5451ed-9cb5-4286-b2e1-02bbf15e0936",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}