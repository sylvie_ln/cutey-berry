{
    "id": "1b885306-4468-4779-aa2b-7ca5561da300",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sB16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8433310f-2dd1-478d-9b1b-ea7da06c8620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b885306-4468-4779-aa2b-7ca5561da300",
            "compositeImage": {
                "id": "7bff5804-e713-472b-8a83-ad0d78ea72e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8433310f-2dd1-478d-9b1b-ea7da06c8620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c27ebf-5276-4257-adea-d441cba6d9d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8433310f-2dd1-478d-9b1b-ea7da06c8620",
                    "LayerId": "a875a65c-854c-493c-93da-bf8a8834cf17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a875a65c-854c-493c-93da-bf8a8834cf17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b885306-4468-4779-aa2b-7ca5561da300",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}