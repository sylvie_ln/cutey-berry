{
    "id": "6b7e2991-ac04-48ef-b10b-169107573981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHearts03",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18848a9c-4f4c-468b-b471-9ab35029dd19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e2991-ac04-48ef-b10b-169107573981",
            "compositeImage": {
                "id": "ae8d0b81-7346-4884-84a9-ab6f65b24397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18848a9c-4f4c-468b-b471-9ab35029dd19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70fb47ef-c704-404f-a511-9cf29d47f4b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18848a9c-4f4c-468b-b471-9ab35029dd19",
                    "LayerId": "99153d2e-417b-41d3-9331-187709976f94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "99153d2e-417b-41d3-9331-187709976f94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b7e2991-ac04-48ef-b10b-169107573981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}