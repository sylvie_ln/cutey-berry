{
    "id": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStarOut",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad9e3e14-086a-4d4f-a447-044aad11c7ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
            "compositeImage": {
                "id": "3501c469-9683-4b88-9722-0d479f9252d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad9e3e14-086a-4d4f-a447-044aad11c7ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cb6a522-0a5c-4c5f-be80-ba555dd0600b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad9e3e14-086a-4d4f-a447-044aad11c7ea",
                    "LayerId": "e1da3b8c-2bb6-4bf0-87ab-bf32b5d56e12"
                }
            ]
        },
        {
            "id": "a6e8f157-65f2-483d-b386-485534a52bf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
            "compositeImage": {
                "id": "12bad81e-ee6f-42ac-914d-347c6c31f82a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6e8f157-65f2-483d-b386-485534a52bf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2240e697-8573-498a-ae20-b7abd17930c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6e8f157-65f2-483d-b386-485534a52bf2",
                    "LayerId": "e1da3b8c-2bb6-4bf0-87ab-bf32b5d56e12"
                }
            ]
        },
        {
            "id": "c9c102cd-062a-4fd4-9dfb-e237881cb04d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
            "compositeImage": {
                "id": "8564247b-b5e4-48d3-92e6-be9158040290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c102cd-062a-4fd4-9dfb-e237881cb04d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c903572-f9f9-4e41-b82c-a4abf4291a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c102cd-062a-4fd4-9dfb-e237881cb04d",
                    "LayerId": "e1da3b8c-2bb6-4bf0-87ab-bf32b5d56e12"
                }
            ]
        },
        {
            "id": "c51c76af-3640-4b7c-8e33-b50e909283d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
            "compositeImage": {
                "id": "002f0d35-e3ae-4d4c-84a5-f86677cdab54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c51c76af-3640-4b7c-8e33-b50e909283d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ed55605-d27f-458a-a011-476526bba103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c51c76af-3640-4b7c-8e33-b50e909283d4",
                    "LayerId": "e1da3b8c-2bb6-4bf0-87ab-bf32b5d56e12"
                }
            ]
        },
        {
            "id": "e38d36e5-e8e5-471a-86d2-870eaf82f757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
            "compositeImage": {
                "id": "3764ada2-1fd0-42c0-be06-92e0f6bb904d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e38d36e5-e8e5-471a-86d2-870eaf82f757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c7653aa-cd4d-4c38-943d-b98b18c34e78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e38d36e5-e8e5-471a-86d2-870eaf82f757",
                    "LayerId": "e1da3b8c-2bb6-4bf0-87ab-bf32b5d56e12"
                }
            ]
        },
        {
            "id": "4f517e07-ca88-4a15-b063-b5eba755814f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
            "compositeImage": {
                "id": "79fd1bf7-5328-458e-a08a-7ad39909294b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f517e07-ca88-4a15-b063-b5eba755814f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee84e4b0-6519-4883-bdb3-7adbedb69174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f517e07-ca88-4a15-b063-b5eba755814f",
                    "LayerId": "e1da3b8c-2bb6-4bf0-87ab-bf32b5d56e12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e1da3b8c-2bb6-4bf0-87ab-bf32b5d56e12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}