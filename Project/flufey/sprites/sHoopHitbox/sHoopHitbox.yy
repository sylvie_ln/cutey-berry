{
    "id": "5c4926cb-14fc-4124-844e-1a7ef9817cbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHoopHitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 6,
    "bbox_right": 26,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cc1f166-3cb0-471d-bbb1-64f5fc03cce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c4926cb-14fc-4124-844e-1a7ef9817cbb",
            "compositeImage": {
                "id": "c38133fe-6e11-44b1-9af0-4477878bc92a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cc1f166-3cb0-471d-bbb1-64f5fc03cce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee15278a-c25c-4090-9cfd-3e57589e1e81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cc1f166-3cb0-471d-bbb1-64f5fc03cce7",
                    "LayerId": "61f66fbe-9865-4eed-838d-79c1d1a0b4ce"
                },
                {
                    "id": "cb77a84e-b099-482d-a09b-a1807921c32a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cc1f166-3cb0-471d-bbb1-64f5fc03cce7",
                    "LayerId": "f36449a9-d74d-4b5c-b6bf-8b2eae44cef7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f36449a9-d74d-4b5c-b6bf-8b2eae44cef7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c4926cb-14fc-4124-844e-1a7ef9817cbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "61f66fbe-9865-4eed-838d-79c1d1a0b4ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c4926cb-14fc-4124-844e-1a7ef9817cbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}