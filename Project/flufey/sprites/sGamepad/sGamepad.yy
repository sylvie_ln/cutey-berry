{
    "id": "d7138eb6-8b76-4d85-be8a-9e9d10d49c03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGamepad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c26252d-9e26-4e67-83e0-a313589d27f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7138eb6-8b76-4d85-be8a-9e9d10d49c03",
            "compositeImage": {
                "id": "5fc64f04-10c3-4662-b215-92adb72ab426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c26252d-9e26-4e67-83e0-a313589d27f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "617e7c8f-a650-40f9-851a-c64f7e1f8b60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c26252d-9e26-4e67-83e0-a313589d27f2",
                    "LayerId": "b280f3fb-64aa-42a4-96b2-5c6e9d4d9541"
                },
                {
                    "id": "bf3d07fc-be08-454a-ac07-c977574ed3bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c26252d-9e26-4e67-83e0-a313589d27f2",
                    "LayerId": "aaf27fe3-9e48-4af3-a57b-fd76e023a01e"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "aaf27fe3-9e48-4af3-a57b-fd76e023a01e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7138eb6-8b76-4d85-be8a-9e9d10d49c03",
            "blendMode": 0,
            "isLocked": false,
            "name": "buttons",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b280f3fb-64aa-42a4-96b2-5c6e9d4d9541",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7138eb6-8b76-4d85-be8a-9e9d10d49c03",
            "blendMode": 0,
            "isLocked": false,
            "name": "pad",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}