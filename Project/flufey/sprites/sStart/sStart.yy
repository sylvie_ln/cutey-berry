{
    "id": "6d78c109-1f24-4b63-9729-5c6d28bb5d08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76c321c5-564b-42b3-a710-e30a2606f0aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d78c109-1f24-4b63-9729-5c6d28bb5d08",
            "compositeImage": {
                "id": "bf1b96ae-43d6-40af-841d-7f65471d0a74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76c321c5-564b-42b3-a710-e30a2606f0aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0259ec96-5ce7-4632-ab18-3942aaf9b013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c321c5-564b-42b3-a710-e30a2606f0aa",
                    "LayerId": "704e2ef8-fb48-4074-8b7c-b881c1848fe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "704e2ef8-fb48-4074-8b7c-b881c1848fe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d78c109-1f24-4b63-9729-5c6d28bb5d08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}