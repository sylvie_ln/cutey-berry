{
    "id": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStarIn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0bf4642-a111-41b8-ab1b-50e993be55d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
            "compositeImage": {
                "id": "c71dffe2-ec38-4393-b09f-4bb3615e8dcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0bf4642-a111-41b8-ab1b-50e993be55d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "921e5f5f-35c6-429b-82a8-3462226aca75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0bf4642-a111-41b8-ab1b-50e993be55d4",
                    "LayerId": "75071d02-be8a-406e-be11-cf78438b5e80"
                }
            ]
        },
        {
            "id": "d506d48f-953e-48b0-9b88-6c36b03ce99b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
            "compositeImage": {
                "id": "5f55549b-2eed-468c-8d72-23a7ec065dce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d506d48f-953e-48b0-9b88-6c36b03ce99b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b288211-7bf8-4442-8174-ed1cb2da9199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d506d48f-953e-48b0-9b88-6c36b03ce99b",
                    "LayerId": "75071d02-be8a-406e-be11-cf78438b5e80"
                }
            ]
        },
        {
            "id": "f9db1c9a-0211-457b-9fc8-5f2fbc2326d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
            "compositeImage": {
                "id": "28a9f336-282f-425a-82b7-8c087e1028d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9db1c9a-0211-457b-9fc8-5f2fbc2326d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c280fa1-8774-4374-852e-9ee2a6c1e07d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9db1c9a-0211-457b-9fc8-5f2fbc2326d5",
                    "LayerId": "75071d02-be8a-406e-be11-cf78438b5e80"
                }
            ]
        },
        {
            "id": "71bd6572-7b94-4d59-b233-0b6cf164e035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
            "compositeImage": {
                "id": "7995a46e-23f5-40dc-8001-606cbafc69b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71bd6572-7b94-4d59-b233-0b6cf164e035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd2321e-527a-4e71-b0c1-f26738a87153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71bd6572-7b94-4d59-b233-0b6cf164e035",
                    "LayerId": "75071d02-be8a-406e-be11-cf78438b5e80"
                }
            ]
        },
        {
            "id": "f5246006-5239-4863-b0af-ca68a2258872",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
            "compositeImage": {
                "id": "6bbdd975-8f73-4fd5-ac06-8620e8661511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5246006-5239-4863-b0af-ca68a2258872",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27f0e21c-2a21-4961-9213-503022b39e77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5246006-5239-4863-b0af-ca68a2258872",
                    "LayerId": "75071d02-be8a-406e-be11-cf78438b5e80"
                }
            ]
        },
        {
            "id": "a01ac811-dc6e-49c0-a616-1d16c70dbe2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
            "compositeImage": {
                "id": "3756d277-ddd2-4a26-a963-f869643d8d65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a01ac811-dc6e-49c0-a616-1d16c70dbe2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4d42a3-d250-41a9-84d4-b172cd4907b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a01ac811-dc6e-49c0-a616-1d16c70dbe2d",
                    "LayerId": "75071d02-be8a-406e-be11-cf78438b5e80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "75071d02-be8a-406e-be11-cf78438b5e80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}