{
    "id": "974db912-f637-49c7-bfa1-4fae36785619",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHoop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7de15bb3-73fd-43c3-9bf8-a2d006bfefed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "974db912-f637-49c7-bfa1-4fae36785619",
            "compositeImage": {
                "id": "2457aea8-5e22-4ab3-a981-90a566bb6c20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7de15bb3-73fd-43c3-9bf8-a2d006bfefed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7c69df-8d4e-4469-a212-8a8f4bdc6d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7de15bb3-73fd-43c3-9bf8-a2d006bfefed",
                    "LayerId": "30ba8112-0cf4-41f9-810b-9a015a1fcd6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "30ba8112-0cf4-41f9-810b-9a015a1fcd6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "974db912-f637-49c7-bfa1-4fae36785619",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}