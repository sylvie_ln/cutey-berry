{
    "id": "1cf5f2a4-e5d2-451f-8777-215a1c2fd502",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sReverser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "621f8669-17d4-440d-a040-0a373b1bba5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cf5f2a4-e5d2-451f-8777-215a1c2fd502",
            "compositeImage": {
                "id": "dbd9be59-dc39-4bf0-8887-6e00a8ba30c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "621f8669-17d4-440d-a040-0a373b1bba5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82e26c9e-05eb-4575-b54e-b412ad660fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "621f8669-17d4-440d-a040-0a373b1bba5a",
                    "LayerId": "419c5bbe-3b48-4e4c-9d4b-dc2a512c07e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "419c5bbe-3b48-4e4c-9d4b-dc2a512c07e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cf5f2a4-e5d2-451f-8777-215a1c2fd502",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}