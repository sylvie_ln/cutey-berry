{
    "id": "94329053-2d70-4d86-b4b1-fb521ad055df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharThrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9575a20-b720-47b2-a893-4d1d5447bdc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94329053-2d70-4d86-b4b1-fb521ad055df",
            "compositeImage": {
                "id": "4f0dfc56-93ab-4526-9f02-ac225c0b7cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9575a20-b720-47b2-a893-4d1d5447bdc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36c12f02-55c4-41d0-95fe-5a0b6a17690d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9575a20-b720-47b2-a893-4d1d5447bdc3",
                    "LayerId": "49ce34da-ddcb-45de-9639-dee7782f75d3"
                }
            ]
        },
        {
            "id": "7f667222-ff94-4bde-b5c1-05ddfdfc8f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94329053-2d70-4d86-b4b1-fb521ad055df",
            "compositeImage": {
                "id": "0dff1712-df4e-4725-906f-81409171d4fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f667222-ff94-4bde-b5c1-05ddfdfc8f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2626e6ba-e1b7-48e6-ac66-21492187b8e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f667222-ff94-4bde-b5c1-05ddfdfc8f84",
                    "LayerId": "49ce34da-ddcb-45de-9639-dee7782f75d3"
                }
            ]
        },
        {
            "id": "48a373e3-8641-4b2e-919b-8db3f9f6786a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94329053-2d70-4d86-b4b1-fb521ad055df",
            "compositeImage": {
                "id": "555ab794-4aaf-4c96-a114-f952f6bf1c2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a373e3-8641-4b2e-919b-8db3f9f6786a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1422410c-0507-4b81-9365-759e3bf7f958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a373e3-8641-4b2e-919b-8db3f9f6786a",
                    "LayerId": "49ce34da-ddcb-45de-9639-dee7782f75d3"
                }
            ]
        },
        {
            "id": "078ecf9a-0053-40d3-8447-3e828e741d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94329053-2d70-4d86-b4b1-fb521ad055df",
            "compositeImage": {
                "id": "95333d02-949b-4111-9a17-9fd3ba40400d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "078ecf9a-0053-40d3-8447-3e828e741d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e706d4b1-3822-4a01-8ede-cdfae5f22e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "078ecf9a-0053-40d3-8447-3e828e741d48",
                    "LayerId": "49ce34da-ddcb-45de-9639-dee7782f75d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "49ce34da-ddcb-45de-9639-dee7782f75d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94329053-2d70-4d86-b4b1-fb521ad055df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}