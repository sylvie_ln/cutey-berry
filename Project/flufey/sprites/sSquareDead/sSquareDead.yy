{
    "id": "afde3ae0-55b0-41e1-b92e-9cfaaa5b9f16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSquareDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "727ba3cc-1d7b-4c2c-9ddd-65a8f8b87c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afde3ae0-55b0-41e1-b92e-9cfaaa5b9f16",
            "compositeImage": {
                "id": "ffc5675b-7890-4e88-b6b6-2607849a530b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "727ba3cc-1d7b-4c2c-9ddd-65a8f8b87c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6571a591-ac7d-4e46-9c5b-2132a814171d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "727ba3cc-1d7b-4c2c-9ddd-65a8f8b87c5b",
                    "LayerId": "c2cdd004-3e39-4bed-82e6-576e438f978d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c2cdd004-3e39-4bed-82e6-576e438f978d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afde3ae0-55b0-41e1-b92e-9cfaaa5b9f16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}