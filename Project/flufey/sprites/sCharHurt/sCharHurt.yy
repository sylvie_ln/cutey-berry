{
    "id": "8c61c652-b0ac-4c45-816c-2f3076c586bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharHurt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a24d21f-a107-44f8-86c9-0789ce27e2d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c61c652-b0ac-4c45-816c-2f3076c586bc",
            "compositeImage": {
                "id": "ed2a6839-466b-41e7-9918-f456ee5fe47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a24d21f-a107-44f8-86c9-0789ce27e2d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec92eec3-b2cb-4f5d-bfa1-08438ff0678d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a24d21f-a107-44f8-86c9-0789ce27e2d8",
                    "LayerId": "4be8eb08-94b1-41dd-97a2-a0b501df4e1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4be8eb08-94b1-41dd-97a2-a0b501df4e1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c61c652-b0ac-4c45-816c-2f3076c586bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}