{
    "id": "81511cc4-b8cd-4ca3-a206-2a081eac0cc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf9a759e-c824-4d10-a933-bcdd1562454a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81511cc4-b8cd-4ca3-a206-2a081eac0cc3",
            "compositeImage": {
                "id": "1646459f-2f72-4ce3-9676-17df202b95c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf9a759e-c824-4d10-a933-bcdd1562454a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55aa2a64-a943-4b44-a486-ac5413e9713f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf9a759e-c824-4d10-a933-bcdd1562454a",
                    "LayerId": "349dc79b-e847-40eb-b4e1-ed1a178e57ec"
                }
            ]
        },
        {
            "id": "caf6f55c-95bc-4b83-8829-27253817a468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81511cc4-b8cd-4ca3-a206-2a081eac0cc3",
            "compositeImage": {
                "id": "17f1b056-8c57-4b4e-8db2-123fde2cba5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caf6f55c-95bc-4b83-8829-27253817a468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ad6d35c-9676-4973-b5d0-371a9913b56b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caf6f55c-95bc-4b83-8829-27253817a468",
                    "LayerId": "349dc79b-e847-40eb-b4e1-ed1a178e57ec"
                }
            ]
        },
        {
            "id": "07f25461-996a-4cc6-a1f4-1787458757c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81511cc4-b8cd-4ca3-a206-2a081eac0cc3",
            "compositeImage": {
                "id": "93f77fcc-c181-4af4-bf16-fa2f54e42df6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07f25461-996a-4cc6-a1f4-1787458757c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb4b7159-59d0-4b3c-80be-f8632892e250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07f25461-996a-4cc6-a1f4-1787458757c0",
                    "LayerId": "349dc79b-e847-40eb-b4e1-ed1a178e57ec"
                }
            ]
        },
        {
            "id": "26b2c4fc-3374-4371-b1e4-c39e6203621a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81511cc4-b8cd-4ca3-a206-2a081eac0cc3",
            "compositeImage": {
                "id": "5e37c1e3-70cf-42a9-9a26-e670d26b11dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26b2c4fc-3374-4371-b1e4-c39e6203621a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0858bd5e-b2dd-4cc6-9ef6-de15c637f6bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26b2c4fc-3374-4371-b1e4-c39e6203621a",
                    "LayerId": "349dc79b-e847-40eb-b4e1-ed1a178e57ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "349dc79b-e847-40eb-b4e1-ed1a178e57ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81511cc4-b8cd-4ca3-a206-2a081eac0cc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}