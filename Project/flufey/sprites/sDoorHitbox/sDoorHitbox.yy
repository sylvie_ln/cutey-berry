{
    "id": "bfd49bf3-9a56-4dfe-b069-63300644452e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sDoorHitbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": -16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3561ef7b-5e11-4836-bc56-7c85969ce6af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd49bf3-9a56-4dfe-b069-63300644452e",
            "compositeImage": {
                "id": "61f1cca5-d437-4f75-8a95-9a8642c42a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3561ef7b-5e11-4836-bc56-7c85969ce6af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4713947-fbe7-4590-a11f-cfb0bac6100f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3561ef7b-5e11-4836-bc56-7c85969ce6af",
                    "LayerId": "740b9889-ab82-4e65-bdcb-6e77d7f51842"
                }
            ]
        },
        {
            "id": "9300d0cd-f44a-41f4-a6d5-af8b4d6ed9a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd49bf3-9a56-4dfe-b069-63300644452e",
            "compositeImage": {
                "id": "9ca8d730-705d-48d1-bdd2-67f38b3ae658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9300d0cd-f44a-41f4-a6d5-af8b4d6ed9a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdcc6037-9391-4225-b9b8-a4df16d3b276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9300d0cd-f44a-41f4-a6d5-af8b4d6ed9a5",
                    "LayerId": "740b9889-ab82-4e65-bdcb-6e77d7f51842"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "740b9889-ab82-4e65-bdcb-6e77d7f51842",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfd49bf3-9a56-4dfe-b069-63300644452e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}