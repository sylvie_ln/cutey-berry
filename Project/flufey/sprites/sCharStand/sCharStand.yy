{
    "id": "27998a6b-fafb-4013-b1f8-f878481884ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCharStand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bb90c37-137b-40c1-9ce6-288bbb424410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27998a6b-fafb-4013-b1f8-f878481884ad",
            "compositeImage": {
                "id": "c0b29e1d-4b50-4a35-8ba8-cf11d1e03daa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb90c37-137b-40c1-9ce6-288bbb424410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e630b84-08bf-4f51-be10-7c3b7bbff1ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb90c37-137b-40c1-9ce6-288bbb424410",
                    "LayerId": "9b8bd379-c4ee-4bac-8e5b-bb62e6458907"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9b8bd379-c4ee-4bac-8e5b-bb62e6458907",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27998a6b-fafb-4013-b1f8-f878481884ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}