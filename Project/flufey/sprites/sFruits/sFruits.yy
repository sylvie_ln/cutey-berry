{
    "id": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFruits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46a3593c-e3b3-412b-b6e4-0952f368dfd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "307b3c53-574f-4582-aee6-98994b3f4a2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46a3593c-e3b3-412b-b6e4-0952f368dfd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c9fa2fb-947a-4bb5-944c-82c86c3f95fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46a3593c-e3b3-412b-b6e4-0952f368dfd1",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "5db2c0e3-6c04-476e-bc0e-24ae6fb481d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "3e5d2b94-eb25-478d-a625-42c7e3126fad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5db2c0e3-6c04-476e-bc0e-24ae6fb481d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ba19852-01bf-40ad-8e38-9fd7fe6501bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5db2c0e3-6c04-476e-bc0e-24ae6fb481d5",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "c42c94c8-3a93-4227-a13c-7ea89d4e785a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "9f0fd77c-c4de-46fc-b6b0-944e220c8ad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c42c94c8-3a93-4227-a13c-7ea89d4e785a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c33f6a08-a73e-4a5a-b7ab-35d81fa5579b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c42c94c8-3a93-4227-a13c-7ea89d4e785a",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "e2e5dffd-d2a7-40e3-8df7-22444277617e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "7bcb7454-2d41-4998-ab21-d3665624fa8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2e5dffd-d2a7-40e3-8df7-22444277617e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491b901c-7b92-4cf7-9471-744d3a80628d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2e5dffd-d2a7-40e3-8df7-22444277617e",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "3f0c0c0b-7c73-49af-9f52-26dd04075822",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "021e57ed-af1c-49b1-8f2e-36e6517dea50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f0c0c0b-7c73-49af-9f52-26dd04075822",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d855328-4854-4cb7-a042-5cdc32b6aa3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f0c0c0b-7c73-49af-9f52-26dd04075822",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "3b2f1127-c407-46c2-91d7-0b8deff4afcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "f848860b-aa71-456e-992b-2215099b5fb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b2f1127-c407-46c2-91d7-0b8deff4afcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb146ac-f7a2-463c-abc2-b3c3c8a478d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2f1127-c407-46c2-91d7-0b8deff4afcc",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "08e3ca9c-6dfc-4ee9-923a-d09795533a0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "2b306653-5641-41f6-9845-884786ea566f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08e3ca9c-6dfc-4ee9-923a-d09795533a0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c81e8e2c-a854-465d-903a-b99451f31048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08e3ca9c-6dfc-4ee9-923a-d09795533a0d",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "94cce4a8-8971-41e1-b5e3-1267f4fb7c3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "3638c9d5-fca9-48bf-aed7-77d38910daef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94cce4a8-8971-41e1-b5e3-1267f4fb7c3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29fe658b-7458-4a48-9906-8a7196cee275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94cce4a8-8971-41e1-b5e3-1267f4fb7c3d",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "9c5fd3b4-dfa9-4bf8-a0f1-e2b4d7bcde65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "c7ef7344-8dd0-4f19-9e8a-87072156d5c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c5fd3b4-dfa9-4bf8-a0f1-e2b4d7bcde65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95829deb-609a-46a3-89c9-cbcc4540617d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c5fd3b4-dfa9-4bf8-a0f1-e2b4d7bcde65",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "a35ee740-3c63-4950-9bc6-0027444dc29f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "d5dbb48f-4b03-4109-8679-9c084f457930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a35ee740-3c63-4950-9bc6-0027444dc29f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "943e5ee7-7102-4f0b-bc6b-e42142ca405d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a35ee740-3c63-4950-9bc6-0027444dc29f",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "fa6f7193-33e8-4a9a-a167-8dd00c06f435",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "09a9691a-ff81-4fb2-adaf-b28b9231c3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa6f7193-33e8-4a9a-a167-8dd00c06f435",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6676636-832b-4add-95fe-1ab53ebaea25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa6f7193-33e8-4a9a-a167-8dd00c06f435",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "685e7527-766d-49b9-a3f7-d28dd583c707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "c5d822ef-4add-4df8-aafa-73507da46971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685e7527-766d-49b9-a3f7-d28dd583c707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1808aec7-d016-4a29-947a-76defe8dbff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685e7527-766d-49b9-a3f7-d28dd583c707",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "2fdf3ea9-a194-4ba4-bc8c-f8b18f5eebdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "e37933ef-70cb-4518-8f22-0c2be0960ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fdf3ea9-a194-4ba4-bc8c-f8b18f5eebdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ce41a6-47a8-4191-9506-03a401eccfe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fdf3ea9-a194-4ba4-bc8c-f8b18f5eebdb",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "0a3b72ce-cff5-4acc-b0c6-0e8ae26f137b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "40831372-075f-4377-a322-535f0e005a97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a3b72ce-cff5-4acc-b0c6-0e8ae26f137b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4f32af-bb6b-4333-a634-5a9909a1105d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a3b72ce-cff5-4acc-b0c6-0e8ae26f137b",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        },
        {
            "id": "6fb15a68-8dc6-43f4-a7e2-37bc85083561",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "compositeImage": {
                "id": "a1bfb8c0-e397-4200-8a24-a089265fd1fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fb15a68-8dc6-43f4-a7e2-37bc85083561",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ad5d113-9e44-442e-9a72-2b589ccc32aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fb15a68-8dc6-43f4-a7e2-37bc85083561",
                    "LayerId": "49077dad-fc09-4472-80f5-47d392e32627"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "49077dad-fc09-4472-80f5-47d392e32627",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4289911551,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4294941179,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}