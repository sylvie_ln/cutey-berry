{
    "id": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sChar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4894f04b-f4b8-4907-8471-02366ff13b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "37d8bcea-8219-4276-a4bc-354d90f6fc5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4894f04b-f4b8-4907-8471-02366ff13b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd8ad3fd-33f1-4f8d-942b-117be5910f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4894f04b-f4b8-4907-8471-02366ff13b22",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "cfdeac68-56b4-4ee2-97de-f6c2dc8a22b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "9ecf491e-f9a5-4f4b-a922-0524beea9a6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfdeac68-56b4-4ee2-97de-f6c2dc8a22b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c36d182e-bb18-48ba-b28b-c06796842710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfdeac68-56b4-4ee2-97de-f6c2dc8a22b2",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "8ac5b842-b7a1-431d-92f9-e0269a2937c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "2c38d165-3fc5-471c-9215-bbb1d947d480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac5b842-b7a1-431d-92f9-e0269a2937c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a72e6d-7a7e-4db9-8466-2bcad1ff34c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac5b842-b7a1-431d-92f9-e0269a2937c7",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "a3b461b6-e319-43c8-9e62-856e8809dcbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "74e649bc-1245-4c41-b0b4-825344dcae00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b461b6-e319-43c8-9e62-856e8809dcbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11aba4a3-2db3-49b9-a3fc-c4c97cddd569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b461b6-e319-43c8-9e62-856e8809dcbb",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "c15dab28-8b0f-453e-8fe7-e4e7f1e6bd09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "ec437ae2-1384-496c-91c6-07de6a7c33ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c15dab28-8b0f-453e-8fe7-e4e7f1e6bd09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b8f766-e8c7-4987-abdc-9c787311c969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c15dab28-8b0f-453e-8fe7-e4e7f1e6bd09",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "15dfdf56-dba2-4a27-a504-3b6f95106424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "c078ad12-9aa6-4566-8e9f-31ed9705d15b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15dfdf56-dba2-4a27-a504-3b6f95106424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95eccf48-5eda-4e4a-92e7-40300c15e8d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15dfdf56-dba2-4a27-a504-3b6f95106424",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "198e6743-140d-4659-a482-721994f59c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "da61e94a-44a5-4e77-b5a5-a7323a2481d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "198e6743-140d-4659-a482-721994f59c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51a96279-d085-4ee5-9ba0-0b79ad907dab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "198e6743-140d-4659-a482-721994f59c4b",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "f2f8b268-2789-4222-bff8-8808de07a444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "c409d7a3-285f-408c-b83b-8f579e30dbf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2f8b268-2789-4222-bff8-8808de07a444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65e9f90f-5b51-49f9-8aa9-85a42b5e7383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2f8b268-2789-4222-bff8-8808de07a444",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "bc40f063-0b7a-4549-b39c-4b01a351d3c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "d61dc99e-b1de-4b2e-bb5d-76eeb9fe26fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc40f063-0b7a-4549-b39c-4b01a351d3c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5976961b-e36c-48d2-abd2-28cdf04478b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc40f063-0b7a-4549-b39c-4b01a351d3c2",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "57b12679-b95f-421f-b74c-3a5b9069cc74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "b5162cb3-3184-4dc2-adf8-5a45efc84ca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b12679-b95f-421f-b74c-3a5b9069cc74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c68f3e-b685-498e-89aa-1467c13caeb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b12679-b95f-421f-b74c-3a5b9069cc74",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "e95b6a8a-ed59-4045-8568-7c5d18884af1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "05d4eeb5-a884-4786-9060-577fb579acaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e95b6a8a-ed59-4045-8568-7c5d18884af1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "495ab68e-e18d-4eff-a27a-b7d9c5ce440f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95b6a8a-ed59-4045-8568-7c5d18884af1",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "2ee8fd4c-d397-44cd-b6bc-01df68f888f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "921b1780-7eef-469d-bec0-9f3edbf44647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee8fd4c-d397-44cd-b6bc-01df68f888f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25314aa-1e83-419f-889e-2543781cb34e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee8fd4c-d397-44cd-b6bc-01df68f888f9",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "8faebcf0-1e03-464c-8962-f41dc115adf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "fc16db60-9604-4fe7-9044-4a26a2ae4b39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8faebcf0-1e03-464c-8962-f41dc115adf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6da3e7b-45d7-472c-b9e1-8285a37e725a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8faebcf0-1e03-464c-8962-f41dc115adf4",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        },
        {
            "id": "2ee67182-d730-4f23-9545-45239b6fcca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "compositeImage": {
                "id": "eecf175d-7c96-4ef2-a925-e281bddef3f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee67182-d730-4f23-9545-45239b6fcca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bffed5f3-5a49-45ca-9f90-326e5c5018e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee67182-d730-4f23-9545-45239b6fcca8",
                    "LayerId": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "04f15e14-a444-4e36-8a8f-5d81fe9c1cb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}