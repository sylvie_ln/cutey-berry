{
    "id": "00706a58-0b83-4759-8831-6b4f3eed884d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sStripey02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "801d3d67-35ea-4913-9919-183760b0b24e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00706a58-0b83-4759-8831-6b4f3eed884d",
            "compositeImage": {
                "id": "77cd97f0-224c-4401-b7a6-cbcd2c17c9a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "801d3d67-35ea-4913-9919-183760b0b24e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcb9e85a-9aaa-40b1-8588-cf57a883fc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "801d3d67-35ea-4913-9919-183760b0b24e",
                    "LayerId": "42f28f9f-983b-42be-bf00-132c919be817"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "42f28f9f-983b-42be-bf00-132c919be817",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00706a58-0b83-4759-8831-6b4f3eed884d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}