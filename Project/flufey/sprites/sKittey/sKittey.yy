{
    "id": "4d6479f8-b620-4482-9770-ace6d9339d05",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sKittey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72226ffb-88db-4edd-b3a4-cd4c1740bbad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
            "compositeImage": {
                "id": "bef8368c-d2c9-4981-8597-fc51b7b9fc54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72226ffb-88db-4edd-b3a4-cd4c1740bbad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c370165-3242-4f99-9248-689e76eafe98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72226ffb-88db-4edd-b3a4-cd4c1740bbad",
                    "LayerId": "d247595f-e821-4ad2-b2d5-2e774c001c85"
                }
            ]
        },
        {
            "id": "fe2a860e-d522-48ef-9d35-5c4db3291710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
            "compositeImage": {
                "id": "604d55f9-acca-4330-b51e-e95617c7c4c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe2a860e-d522-48ef-9d35-5c4db3291710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f408b50-01e9-41eb-9b09-d18b2c0085db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe2a860e-d522-48ef-9d35-5c4db3291710",
                    "LayerId": "d247595f-e821-4ad2-b2d5-2e774c001c85"
                }
            ]
        },
        {
            "id": "a6584992-09e1-4b89-a860-b5687574bfc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
            "compositeImage": {
                "id": "6a41b29c-6a78-43a0-a2d4-4a39e6a89736",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6584992-09e1-4b89-a860-b5687574bfc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbaf10a1-af6b-4651-9429-3878ff2cb19b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6584992-09e1-4b89-a860-b5687574bfc1",
                    "LayerId": "d247595f-e821-4ad2-b2d5-2e774c001c85"
                }
            ]
        },
        {
            "id": "04bc5d41-0222-4609-be6c-885360d5588f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
            "compositeImage": {
                "id": "3b8b4890-b329-4260-afe8-90240b49df74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04bc5d41-0222-4609-be6c-885360d5588f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44f1d8a9-b984-47d2-a9ad-e064b75f62fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04bc5d41-0222-4609-be6c-885360d5588f",
                    "LayerId": "d247595f-e821-4ad2-b2d5-2e774c001c85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d247595f-e821-4ad2-b2d5-2e774c001c85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294437317,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}