var collision = argument[0];
var process = true;
if argument_count > 1 {
	process = argument[1];	
}
var all_collisions = collision[1];
var filtered_collisions = collision[2];
var overlapping_actors = collision[3];
var adjacent_actors = collision[4];

var stop = handle_collision_basic([true,all_collisions],false);
handle_collision_basic([true,filtered_collisions],process);
if process {
	var processed = ds_map_create();
	for(var i=0; i<ds_list_size(overlapping_actors); i++) {
		var actor = overlapping_actors[|i];
		if !ds_map_exists(processed,actor) {
			ds_map_add(processed,actor,true);
			script_execute(process_collision,actor);
		}
	}
	for(var i=0; i<ds_list_size(adjacent_actors); i++) {
		var actor = adjacent_actors[|i];
		if array_length_1d(hurt_adjacent) != 0 or  array_length_1d(actor.hurt_adjacent) != 0 {
			if !ds_map_exists(processed,actor) {
				ds_map_add(processed,actor,true);
				script_execute(process_collision,actor);
			}
		}
	}
	ds_map_destroy(processed);
}

ds_list_destroy_cache(overlapping_actors);
ds_list_destroy_cache(adjacent_actors);
return stop;