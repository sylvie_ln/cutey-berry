var collision = argument[0];
var actors = collision[1];
var axis = argument[1];
var dir = argument[2];
for(var i=0; i<ds_list_size(actors); i++) {
	var actor = actors[|i];
	if !object_is_ancestor(actor.object_index,oActor) { continue; }
	if !actor.carryable[axis] { continue; }
	actor.mark_as_riding[axis] = id;
	if actor.mark_as_carried[axis] != noone and actor.mark_as_carried[axis] != id { continue; }
	if actor.bbox_bottom < bbox_top {
		with actor {
			if axis == 1 and dir < 0 { break; }
			if dir == 0 { break; }
			if global.move_debug {
				show_debug_message(
				string(bbox_bottom)+" "+
				string(other.bbox_top)+" "+
				object_get_name(other.object_index)+" carrying "+object_get_name(actor.object_index)+" "+string(dir)+" on axis "+string(axis));
			}
			move(dir,axis);	
			actor.mark_as_carried[axis] = other.id;
			if axis == 0 {
				actor.mhv = other.hv;	
			}
			if axis == 1 {
				actor.mvv = 0;
			}
		}
	}
}
script_execute(handle_collision,collision,false)