///@param x
///@param y
///@param obj
///@param list
///@param ordered
var xp = argument[0];
var yp = argument[1];
var obj = argument[2];
var list = argument[3];
var ordered = argument[4];
var temp = ds_list_create_cache();
var xo = [1,-1,0,0];
var yo = [0,0,1,-1];
for(var i=0; i<4; i++) {
	ds_list_clear(temp);
	instance_place_list(xp+xo[i],yp+yo[i],obj,temp,ordered);
	for(var j=0; j<ds_list_size(temp); j++) {
		ds_list_add(list,temp[|j]);	
	}
}
ds_list_destroy_cache(temp);
return ds_list_size(list);
