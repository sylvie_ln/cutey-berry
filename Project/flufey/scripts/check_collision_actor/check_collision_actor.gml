var collision = check_collision_basic(argument[0],argument[1]);
var all_collisions = collision[1];
var filtered_collisions = ds_list_create_cache();
var overlapping_actors = ds_list_create_cache();
var adjacent_actors = ds_list_create_cache();
for(var i=0; i<ds_list_size(all_collisions); i++) {
	var inst = all_collisions[|i];
	if object_is_ancestor(inst.object_index,oActor) {
		ds_list_add(overlapping_actors,inst);
	} else {
		ds_list_add(filtered_collisions,inst);	
	}
}
var num	= instance_adjacent_list_actor(argument[0],argument[1],oActor,adjacent_actors,false);
return [collision[0] or (num != 0),all_collisions,filtered_collisions,overlapping_actors,adjacent_actors];
