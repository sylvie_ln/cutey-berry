var ax1 = argument[0];
var ay1 = argument[1];
var ax2 = argument[2];
var ay2 = argument[3];
var bx1 = argument[4];
var by1 = argument[5];
var bx2 = argument[6];
var by2 = argument[7];
return (ax1 < bx2 && ax2 > bx1 && ay1 < by2 && ay2 > by1);