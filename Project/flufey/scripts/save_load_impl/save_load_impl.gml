var map = argument[1];
var type = argument[2]

if argument[0] == "save" {
	global_script = set_map_global;
	key_script = save_keys;
}
if argument[0] == "load" {
	global_script = get_map_global;
	key_script = load_keys;
}

if type == "game" {
	for(var i=0; i<global.total_levels; i++) {
		variable_global_set("complete_"+string(i),global.complete[i]);
		script_execute(global_script,"complete_"+string(i),map);
		global.complete[i] = variable_global_get("complete_"+string(i));
	}
} else if type == "options" {
	script_execute(global_script,"scale",map);
	script_execute(global_script,"volume",map);
	script_execute(key_script,map);
}

return map;