var inst = argument[0];
stop = false;
if !instance_exists(inst) { return stop; }
if inst.solid {
	stop = true;	
}
if solid and object_is_ancestor(inst.object_index,oActor) {
	stop = true;	
}
if object_is_ancestor(inst.object_index,oUpfloor) {
	if bbox_bottom < inst.bbox_top {
		stop = true;	
	}
}
return stop;