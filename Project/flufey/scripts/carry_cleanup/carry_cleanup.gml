var axis = argument[0];
with oActor {
	if mark_as_riding[axis] == other.id {
		riding_this_step[axis] = true;	
	}
	if mark_as_carried[axis] == other.id {
		carried_this_step[axis] = true;
	}
}