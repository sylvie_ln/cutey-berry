var actor = argument[0]; 
var axis = argument[1];
var dir = argument[2];

if object_is_ancestor(object_index,oUpfloor) {
	if !(actor.bbox_bottom < bbox_top) {
		return false;	
	}
}

if axis == 0 {
	if dir > 0 {
		if bbox_right < actor.bbox_left {
			return true;
		}
	} else if dir < 0 {
		if actor.bbox_right < bbox_left {
			return true;
		}
	}
} else if axis == 1 {
	if dir > 0 {
		if bbox_bottom < actor.bbox_top {
			return true;
		}
	} else if dir < 0 {
		if actor.bbox_bottom < bbox_top {
			return true;
		}
	}
}
return false;