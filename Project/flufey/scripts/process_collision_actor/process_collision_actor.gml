var inst = argument[0];
if object_is_ancestor(inst.object_index,oActor) {
	// actor moved into inst!
	with inst {
		// inst takes damage if it's beside one of actor's hurt sides
		var hurt = false;
		for(var i=0; i<array_length_1d(other.hurt_sides); i++) {
			var side = other.hurt_sides[i];
			hurt = hurt or beside(other,side[0],-side[1]);
		}
		if hurt {
			event_user(eActor.take_damage);	
		}
		with other {
			// actor takes damage if it's beside one of inst's hurt sides
			if !instance_exists(inst) { break; }
			var hurt = false;
			for(var i=0; i<array_length_1d(inst.hurt_sides); i++) {
				var side = inst.hurt_sides[i];
				hurt = hurt or beside(inst,side[0],-side[1]);
			}
			if hurt {
				event_user(eActor.take_damage);	
			}
		}
	}
}