// Splits a single string into a list of strings according to the given separator.
// If an occurrence of the separator is prefixed with a backslash, the string will not
// be split at that occurrence and the backslash will be omitted from the result.
// Remember to destroy the returned ds_list when you are done with it.
// Example: 
// String is "abc,def,g\,h\,i"
// Separator is ","
// Result is a list containing "abc", "def" and "g,h,i" in that order.
// Arguments:
// 0: The string to split
// 1: The separator
var str = argument[0];
var sep = argument[1];
var token = "";
var list = ds_list_create_cache();
for(var i=1; i<=string_length(str)-string_length(sep)+1; i++) {
    if string_copy(str,i,string_length(sep)+1) = "\\"+sep {
        i += string_length(sep);
        token += sep;
        continue;
    }
    if string_copy(str,i,string_length(sep)) == sep {
        ds_list_add(list,token);
        token = "";
        i += string_length(sep)-1;
        continue;
    }
    token += string_char_at(str,i);
}
if i <= string_length(str) {
    token += string_extract(str,i,string_length(str));
}
if token != "" {
    ds_list_add(list,token);
}
return list;