var collision = argument[0];
var list = collision[1];
var process = true;
if argument_count > 1 {
	process = argument[1];	
}
var stop = false;
for(var i=0; i<ds_list_size(list); i++) {
	var inst = list[|i];
	if process {
		script_execute(process_collision,inst);	
	}
	stop = stop or solid_check(inst);
}	
ds_list_destroy_cache(list);
return stop;