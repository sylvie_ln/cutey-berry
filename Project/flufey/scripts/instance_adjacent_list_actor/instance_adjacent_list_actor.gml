///@param x
///@param y
///@param obj
///@param list
///@param ordered
var xp = argument[0];
var yp = argument[1];
var obj = argument[2];
var list = argument[3];
var ordered = argument[4];
var temp = ds_list_create_cache();
var xo = [1,-1,0,0];
var yo = [0,0,1,-1];
for(var i=0; i<4; i++) {
	var axis = 0;
	if yo != 0 { axis = 1; }
	if axis == 0 {
		var dir = xo[i];	
	} else if axis == 1 {
		var dir = yo[i];
	}
	var found_hurt_side = false;
	for(var j=0; j<array_length_1d(hurt_adjacent); j++) {
		var hs = hurt_adjacent[j];
		if hs[0] == axis and hs[1] == dir {
			found_hurt_side = true;	
		}
	}
	if !found_hurt_side { continue; }
	ds_list_clear(temp);
	instance_place_list(xp+xo[i],yp+yo[i],obj,temp,ordered);
	for(var j=0; j<ds_list_size(temp); j++) {
		ds_list_add(list,temp[|j]);	
	}
}
ds_list_destroy_cache(temp);
return ds_list_size(list);
