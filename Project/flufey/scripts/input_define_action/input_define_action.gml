///@description Defines an action name for which input can be detected.
///@param action_name String representing the action name.
var action = argument[0];
ds_map_add_list(oInput.actions,string(action),ds_list_create_cache());
ds_list_add(oInput.action_list,string(action));
ds_map_add(oInput.held_time_map,string(action),0);