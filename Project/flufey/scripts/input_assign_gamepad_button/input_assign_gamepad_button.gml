///@description Assigns a gamepad button to the given action.
///@param action_name String representing the action name.
///@param buttons... Buttons to assign to the action.
var action = argument[0];
var list = oInput.actions[? action];
for(var i=1; i<argument_count; i++) {
	var button = argument[i];
	var was_waiting = false;
	for(var j=0; j<ds_list_size(list); j++) {
		var input = list[|j];
		if input[|0] == input_kind.waiting {
			input[|0] = input_kind.gamepad_button;
			ds_list_add(input,button);
			was_waiting = true; 
			break;
		}
	}
	if !was_waiting {
		ds_list_add(list,list_create(input_kind.gamepad_button,button));
	}
}