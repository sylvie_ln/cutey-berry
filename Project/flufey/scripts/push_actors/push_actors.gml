var collision = script_execute(check_collision,argument[0],argument[1]);
var axis = argument[2];
var dir = argument[3];
var actors = collision[1];
if collision[0] {
	for(var i=0; i<ds_list_size(actors); i++) {
		var actor = actors[|i];
		if !object_is_ancestor(actor.object_index,oActor) { continue; }
		if !actor.pushable[axis] { continue; }
		if beside(actor,axis,dir) {
			with actor {
				if axis == 1 and dir > 0 { break; }
				if global.move_debug {
					show_debug_message(
					string(bbox_bottom)+" "+
					string(other.bbox_top)+" "+
					object_get_name(other.object_index)+" pushing "+object_get_name(actor.object_index)+" "+string(dir)+" on axis "+string(axis));
				}
				if !move(dir,axis) {
					with other {
						script_execute(on_collide,axis);
						collided[axis] = true;
					}
				}
				if axis == 1 {
					actor.mvv = -power(abs(other.vv),0.75);	
					actor.vv = 0;
				}
			}
		}
	}
}
script_execute(handle_collision,collision);