if global.list_cache_front >= global.list_cache_size {
	for(var i=0; i<global.list_cache_size; i++) {
		if !ds_map_exists(global.list_cache_map,global.list_cache[i]) {
			ds_map_add(global.list_cache_map,global.list_cache[i],i);
			if global.list_cache_debug {
				show_debug_message("allocating "+string(i));
			}
			return global.list_cache[i];
		}
	}	
	show_error("List cache is full!",true);
} else {
	if global.list_cache_debug {	
		show_debug_message("allocating "+string(global.list_cache_front));
	}
	var list = global.list_cache[global.list_cache_front];	
	ds_map_add(global.list_cache_map,list,global.list_cache_front);
	global.list_cache_front++;
	if global.list_cache_front < global.list_cache_size 
	and ds_map_exists(global.list_cache_map,global.list_cache[global.list_cache_front]) {
		global.list_cache_front = global.list_cache_size;	
	}
	return list;
}