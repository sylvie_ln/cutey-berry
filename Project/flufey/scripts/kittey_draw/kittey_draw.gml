if outside_view() { exit; }
draw_outline_ext(c_black,1);
draw_sprite_ext(sKitteyBase,image_index,round(x),round(y),image_xscale,image_yscale,image_angle,base_color,image_alpha);
draw_sprite_ext(sKitteyHighlight,image_index,round(x),round(y),image_xscale,image_yscale,image_angle,highlight_color,image_alpha);