if outside_view() and object_is_ancestor(object_index,oActor) {
	return 0;	
}
if argument_count > 1 {
	audio_sound_pitch(argument[0],1+random_range(-argument[1],argument[1]));
}
audio_play_sound(argument[0],100,false);