var index = global.list_cache_map[? argument[0]];
ds_list_clear(global.list_cache[index]);
ds_map_delete(global.list_cache_map,argument[0]);
global.list_cache_front = index;
if global.list_cache_debug {		
	show_debug_message("freeing "+string(index));
}