var amount = argument[0];
var axis = argument[1];
amount = clamp(amount,-12,12);

if global.move_debug {
	global.move_calls++;
	var move_call = global.move_calls;
	var pos = [x,y];
	show_debug_message(object_get_name(object_index)+ " is moving! "+string(move_call)+" "+string(pos[axis]));
}

remainder[axis] += amount;
var target = round_ties_down(remainder[axis]);
remainder[axis] -= target;

var xa = (axis == 0) ? 1 : 0;
var ya = (axis == 1) ? 1 : 0;
var s = sign(target);
var go = true;

if target == 0 { // if we aren't moving, we still need to do some stuff...	
	// check and handle collisions at our position
	var collision = script_execute(check_collision,x,y);
	if collision[0] {
		var stop = script_execute(handle_collision,collision);
		if stop {
			go = false;
		}
	} else {
		script_execute(handle_collision,collision,false);	
	}
	// run carry stuff to update riders
	if can_carry[axis] {
		var collision = carry_pre_step();
		carry_post_step(collision,axis,0);	
	}
}

while target != 0 {
	
	if can_push[axis] {
		push_actors(x+xa*s,y+ya*s,axis,s);	
	}
	
	var collision = script_execute(check_collision,x+xa*s,y+ya*s);
	if collision[0] {
		var stop = script_execute(handle_collision,collision,false);
		if stop {
			go = false;
		}
	} else {
		script_execute(handle_collision,collision,false);	
	}

	
	if can_carry[axis] {
		var carry_list = carry_pre_step();
	}
	
	if go {
		x += xa*s;
		y += ya*s;
		target -= s;
	}
	
	if can_carry[axis] {
		if go {
			carry_post_step(carry_list,axis,s);	
		} else {
			carry_post_step(carry_list,axis,0);	
		}
	}
	
	if !go { break; }
}

if can_carry[axis] {
	carry_cleanup(axis);
}

if !go and !collided[axis] {
	script_execute(on_collide,axis);	
	collided[axis] = true;
}

if global.move_debug {
	var pos = [x,y];
	show_debug_message(object_get_name(object_index)+ " done moving... "+string(move_call)+" "+string(pos[axis]));
}

return go;