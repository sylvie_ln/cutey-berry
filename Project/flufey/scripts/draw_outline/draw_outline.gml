var size = 2*max(npow2(abs(sprite_width)+abs(image_xscale*2)),npow2(abs(sprite_height)+abs(image_yscale*2)));
var surfy = get_scratch_surface(size);
surface_set_target(surfy);
draw_clear_alpha(c_black,0);
var xp = abs(sprite_xoffset)+abs(image_xscale);
var yp = abs(sprite_yoffset)+abs(image_yscale);
var prev_image_alpha = image_alpha;
var prev_image_blend = image_blend;
image_alpha = 1;
image_blend = c_white;
draw_silhouette(xp-image_xscale,yp);
draw_silhouette(xp+image_xscale,yp);
draw_silhouette(xp,yp-image_yscale);
draw_silhouette(xp,yp+image_yscale);
image_alpha = prev_image_alpha;
image_blend = prev_image_blend;
surface_reset_target();

if argument_count >= 2 {
    xp = argument[0];
    yp = argument[1];
} else {
    xp = round(x);
    yp = round(y);
}

draw_surface_ext(
surfy,
xp-(abs(sprite_xoffset)+abs(image_xscale)),
yp-(abs(sprite_yoffset)+abs(image_yscale)),
1,1,0,image_blend,image_alpha);