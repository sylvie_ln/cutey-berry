if target != noone and instance_exists(target) {
	ds_list_add(positions,[target.x,target.y]);
	if ds_list_size(positions) >= delay {
		var pos = positions[|0];
		x = pos[0];
		y = pos[1];
		ds_list_delete(positions,0);
		if sign(x-xprevious) != 0 {
			image_xscale = sign(x-xprevious);
		}
	}
}