{
    "id": "4d5a42cf-2262-471c-9fd5-4d44fd503871",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oKittey",
    "eventList": [
        {
            "id": "9bd0dc43-efaf-43d9-9c44-d78105c28f83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d5a42cf-2262-471c-9fd5-4d44fd503871"
        },
        {
            "id": "3de013dd-180f-4eea-ac17-786339a3444d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4d5a42cf-2262-471c-9fd5-4d44fd503871"
        },
        {
            "id": "3d415147-2c52-46e0-9cc8-033f06c3dbf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d5a42cf-2262-471c-9fd5-4d44fd503871"
        },
        {
            "id": "09ec868d-68e5-4b24-ac8c-05dacf1ade1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4d5a42cf-2262-471c-9fd5-4d44fd503871"
        },
        {
            "id": "f0faf7b9-0a28-4322-ae4f-b1165a7745f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "4d5a42cf-2262-471c-9fd5-4d44fd503871"
        }
    ],
    "maskSpriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
    "visible": true
}