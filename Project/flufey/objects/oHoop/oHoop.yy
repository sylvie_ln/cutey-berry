{
    "id": "46cfdc73-9996-4060-ac90-e4fa9162cbaf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHoop",
    "eventList": [
        {
            "id": "41407ed8-ad20-4253-b0c2-003e5da386ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46cfdc73-9996-4060-ac90-e4fa9162cbaf"
        },
        {
            "id": "1fab5b42-e624-4b5c-9f28-8261fe5f881b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46cfdc73-9996-4060-ac90-e4fa9162cbaf"
        },
        {
            "id": "3a0f8950-793a-46fa-81a5-6b08bbe7a86d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "46cfdc73-9996-4060-ac90-e4fa9162cbaf"
        },
        {
            "id": "048bc282-8a34-473b-99de-8ce5e6937c27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "46cfdc73-9996-4060-ac90-e4fa9162cbaf"
        },
        {
            "id": "2d15320a-45d3-4289-878f-98adc4b3b5b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "46cfdc73-9996-4060-ac90-e4fa9162cbaf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "974db912-f637-49c7-bfa1-4fae36785619",
    "visible": true
}