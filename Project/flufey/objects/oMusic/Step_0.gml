if room == rmEnding {
	if !audio_is_playing(bgmBeryEnding) {
		audio_sound_gain(bgmBeryEnding,0,0)
		audio_sound_gain(bgmBeryEnding,0.5,12000);
		bgm_play(bgmBeryEnding);	
	}
	exit;	
}
if !audio_is_playing(bgmBeryIntro) and !audio_is_playing(bgmBery) {
	if room != rmTitle and room != rmOptions {
		bgm_play(bgmBery);	
	} else {
		sfx_play(bgmBeryIntro);
	}
}