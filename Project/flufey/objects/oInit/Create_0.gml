global.list_cache_size = 512;
global.list_cache_front = 0;
for(var i=0; i<global.list_cache_size; i++) {
	global.list_cache[i] = ds_list_create();	
}
global.list_cache_map = ds_map_create();
global.list_cache_debug = false;


global.sylvie_font = font_add_sprite(sSylvieFont,ord(" "),true,1);
global.sylvie_font_bold = font_add_sprite(sSylvieFontB,ord(" "),true,1);
global.sylvie_font_bold_2x = font_add_sprite(sSylvieFontB2x,ord(" "),true,2);

global.game_save_map = ds_map_create();
global.options_save_map = ds_map_create();
global.game_save_file = "save.fluffy"
global.options_save_file = "options.fluffy"

init_outline();

global.view_width = room_width;
global.view_height = room_height;

global.max_scale = compute_max_scale();

global.scale = global.max_scale;
global.volume = 1;

instance_create_depth(0,0,0,oInput);

load_options();

global.scale = min(global.scale,global.max_scale);
window_set_scale(global.scale);
audio_set_master_gain(0,global.volume)

global.stepc = 0;
global.move_calls = 0;
global.move_debug = false; 
global.bgm = -1;

global.depth_way_above = -90;
global.depth_above = -50;
global.depth_middle = 0;
global.depth_below = 50;
global.depth_way_below = 90

global.level = 0;

global.win = false;
global.door = -1;

global.total_levels = 7; //sprite_get_number(sFruits);
for(var i=0; i<global.total_levels; i++) {
	global.complete[i] = false;	
}

global.spawn_x = -1;
global.spawn_y = -1;

load_game();

instance_create_depth(x,y,depth,oMusic);

alarm[0] = 1;