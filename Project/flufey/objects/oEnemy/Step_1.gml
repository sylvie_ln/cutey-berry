event_inherited();
if hit {
	sprite_index = asset_get_index(basename()+"Hit");
} else {
	sprite_index = object_get_sprite(object_index);
}
if dead and !knockback {
	instance_destroy();	
}