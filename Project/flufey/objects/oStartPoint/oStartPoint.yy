{
    "id": "bf01df85-1dc7-45a1-a153-0937dfcd3b20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStartPoint",
    "eventList": [
        {
            "id": "19b1bcb6-d887-42bb-9bbe-9bea873af9d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "bf01df85-1dc7-45a1-a153-0937dfcd3b20"
        },
        {
            "id": "e2d17ec2-5540-4bb7-a700-7fc4e91bad90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf01df85-1dc7-45a1-a153-0937dfcd3b20"
        },
        {
            "id": "d2dfc9e2-499c-4f56-a0a6-167ccbe87f41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf01df85-1dc7-45a1-a153-0937dfcd3b20"
        },
        {
            "id": "5127cf2d-cace-47ae-b2f6-48efab9d6473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "bf01df85-1dc7-45a1-a153-0937dfcd3b20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d78c109-1f24-4b63-9729-5c6d28bb5d08",
    "visible": false
}