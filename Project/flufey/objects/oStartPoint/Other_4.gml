if room == rmMap {
	if global.spawn_x != -1 { x = global.spawn_x }
	if global.spawn_y != -1 { y = global.spawn_y }
}
var stars = 12;
sfx_play(sfxStarIn);
var anim_length = sprite_get_number(sStarIn)*(room_speed/sprite_get_speed(sStarIn));
for(var i=0; i<stars; i++) {
	var spd = 3;
	var dir = (360/stars)*i;
	var star = instance_create_depth(x+lengthdir_x(spd*anim_length,dir),y+lengthdir_y(spd*anim_length,dir),depth,oStarIn);
	star.speed = spd;
	star.direction = dir+180;
	with star { event_user(0); }
}