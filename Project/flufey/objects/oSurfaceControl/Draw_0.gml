size = npow2(max(room_width,room_height));

if !surface_exists(stencil) {
	stencil = surface_create(size,size);
	surface_set_target(stencil);
	draw_clear_alpha(c_black,0);
	with oBlock {		
		draw_set_color(c_black);
		var w = sprite_width;
		var h = sprite_height;
		var b = 1;
		if os_browser != browser_not_a_browser {
			draw_rectangle(x-b-w/2,y-b-h/2,x+w/2+b,y+h/2+b,false);
		} else {
			draw_rectangle(x-b-w/2,y-b-h/2,x+w/2-1+b,y+h/2-1+b,false);	
		}
	}
	with oBlock {
		draw_set_color(c_white);
		var w = sprite_width;
		var h = sprite_height;
		var b = 0;
		if os_browser != browser_not_a_browser {
			draw_rectangle(x-b-w/2,y-b-h/2,x+w/2+b,y+h/2+b,false);
		} else {
			draw_rectangle(x-b-w/2,y-b-h/2,x+w/2-1+b,y+h/2-1+b,false);	
		}
	}
	surface_reset_target();
}
if surface_exists(stencil) and!surface_exists(surfy) {
	surfy = surface_create(size,size);
	surface_set_target(surfy);
	draw_clear_alpha(c_black,0);
	draw_sprite_tiled_ext(bg,0,0+x_offset,0+y_offset,bg_scale,bg_scale,image_blend,image_alpha);
	gpu_set_blendmode_ext(bm_dest_color,bm_zero);
	draw_surface(stencil,0,0);
	gpu_set_blendmode(bm_normal);
	surface_reset_target();
}

if surface_exists(surfy) {
	draw_surface(surfy,0,0);	
}
