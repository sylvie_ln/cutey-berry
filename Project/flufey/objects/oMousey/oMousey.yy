{
    "id": "3f40d5b8-b8cb-48ec-ab40-67edffeac8aa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMousey",
    "eventList": [
        {
            "id": "4bcbfd08-bb71-4407-aec9-c03a8d32e72f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3f40d5b8-b8cb-48ec-ab40-67edffeac8aa"
        },
        {
            "id": "f7c86e4a-35c1-44f8-a00d-b401750e8bd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3f40d5b8-b8cb-48ec-ab40-67edffeac8aa"
        },
        {
            "id": "2486287f-1ac8-4a8d-9f6b-737be074e351",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f40d5b8-b8cb-48ec-ab40-67edffeac8aa"
        },
        {
            "id": "92b38622-0f96-4ba4-9f37-892893a57d72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "3f40d5b8-b8cb-48ec-ab40-67edffeac8aa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}