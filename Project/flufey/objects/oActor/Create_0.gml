event_inherited();
remainder = [0,0];
check_collision = check_collision_actor;
handle_collision = handle_collision_actor;
process_collision = process_collision_actor;
on_collide = stop_movement;

collided = [false,false];

hdamp = 1;
vdamp = 1;

hv = 0;
vv = 0;
grav = 0.5;

mhv = 0;
mvv = 0;

khv = 0;
kvv = 0;
kspd = 6;
kjmp = 4;
kfrc = grav;
knockback = false;
knockback_timer = 0;
knockback_time = room_speed div 2;

can_push = [false,false];
can_carry = [false,false];
pushable = [false,false];
carryable = [false,false];
carried_this_step = [false,false];
mark_as_carried = [noone,noone];
riding_this_step = [false,false];
mark_as_riding = [noone,noone];
riding_previous_step = [false,false];
disembark = false;

hp = 1;
damage = 0;
team = actor_teams.none;
cannot_be_damaged = false;
hit = false;
hit_this_frame = false;
hit_timer = 0;
hit_time = room_speed div 2;
hitflash_timer = 0;
hitflash_time = room_speed div 10;

prev_image_alpha = image_alpha;

hurt_sides = [[0,-1],[0,1],[1,-1],[1,1]]; // axis,dir pairs
hurt_adjacent = hurt_sides; 

paused = false;

enum actor_teams {
	player,
	enemy,
	none,
}

enum eActor {
	take_damage = 10,
	die = 11,
}