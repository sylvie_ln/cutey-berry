if paused { exit; }

if disembark or (riding_previous_step[0] and !riding_this_step[0]) {
	if global.move_debug {
		show_debug_message(object_get_name(object_index)+" disembarked on axis 0");	
	}
	var s = sign(hv);
	hv += mhv;
	if s != 0 and s != sign(hv) {
		hv = 0;	
	}
	mhv = 0;
}
if disembark or (riding_previous_step[1] and !riding_this_step[1]) {
	if global.move_debug {
		show_debug_message(object_get_name(object_index)+" disembarked on axis 1");	
	}
	vv += mvv;
	mvv = 0;	
}

if knockback {
	var s = sign(khv);
	khv -= kfrc*s;
	if s != sign(khv) {
		khv = 0;	
	}
	var s = sign(kvv);
	kvv -= kfrc*s;
	if s != sign(kvv) {
		kvv = 0;	
	}
	move(khv,0);
	move(kvv,1);
} 