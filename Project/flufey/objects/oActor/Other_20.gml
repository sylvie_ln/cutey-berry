/// @description Take Damage
if !cannot_be_damaged and !hit {
	if team != other.team and other.damage != 0 {
		hp -= other.damage;
		hit = true;
		hit_this_frame = true;
		hit_timer = hit_time;
		hitflash_timer = hitflash_time;
		knockback_timer = knockback_time;
		knockback = true;
		hv = 0;
		vv = 0;
		khv = kspd*sign(x-other.x);
		if khv == 0 {
			khv = -kspd;	
		}
		kvv = -kjmp;
		if hp <= 0 {
			event_user(eActor.die);	
		}
	}
}