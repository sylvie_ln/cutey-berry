{
    "id": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oActor",
    "eventList": [
        {
            "id": "ceeae8d4-c8df-46ce-841c-107d6eda5f31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "02447ab9-57a8-479a-960e-b22144d3cfe6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "976a3705-2ca3-42b7-89d5-dff8e4e7afb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "f7051582-6612-4728-8630-e5ab6a143980",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "a184af28-b946-4cd9-a0a0-d8e05f00cf2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "c3c65534-cbc3-4f1f-a631-ee3d8e95a843",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 21,
            "eventtype": 7,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        },
        {
            "id": "d8d49fdf-607f-4d81-aad4-817d230db134",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}