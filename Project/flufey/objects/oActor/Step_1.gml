collided = [false,false]
riding_previous_step = riding_this_step;
riding_this_step = [false,false];
mark_as_riding = [noone,noone];
disembark = false;
carried_this_step = [false,false];
mark_as_carried = [noone,noone];
//mhv = 0;
//mvv = 0;

hit_this_frame = false;
if hit {
	if hit_timer > 0 {
		hit_timer--;
		if hit_timer <= 0 {
			hit = false;	
		}
	}
	if hitflash_timer == hitflash_time {
		hitflash_timer = 0;
		prev_image_alpha = image_alpha;
		image_alpha = 0;
	} else {
		hitflash_timer++;
		image_alpha = prev_image_alpha;
	}
}
if knockback {
	knockback_timer--;
	if knockback_timer <= 0 {
		knockback = false;	
	}
}