if paused { exit; }
if target != noone and instance_exists(target) and !knockback {
	var ong = on_ground();
	
	if abs(target.y - y) < 20 {
		dir = sign(target.x-x);
	} else {
		if hv != 0 {
			dir = sign(hv);
		}
	}
	
	hv += dir*acc;
	if abs(hv) > spd {
		hv = spd*dir;	
	}
	
	if !ong {
		vv += grav;
	}
	
	jump_timer--;
	if jump_timer <= 0 and ong {
		jump_timer = jump_time;
		vv = -jmp;
		sfx_play(sfxSquareJump);
	}
	
	if place_meeting(x+hv,y,oReverser) {
		hv = -hv;	
	}
	
	move(hv,0);
	move(vv,1);
}

if hv != 0 {
	image_xscale = sign(hv);
}