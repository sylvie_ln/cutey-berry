event_inherited();
spd = 2;
acc = 0.2;
jmp = sqrt(2*grav*17);
target = oChar;
dir = 1;

hp = 2;
damage = 1;
hurt_sides = [[0,-1],[0,1],[1,1]]
hurt_adjacent = [[1,1]];

can_carry = [true,true];
can_push = [true,true];
pushable = [false,true];
carryable = [false,true];

on_collide = bounce_hori;

jump_time = room_speed;
jump_timer = irandom(jump_time);