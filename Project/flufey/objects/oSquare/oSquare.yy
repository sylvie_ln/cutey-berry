{
    "id": "252ddd11-30a7-41cd-8eb1-5438543f1525",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSquare",
    "eventList": [
        {
            "id": "a667bf3a-e310-48ad-81d6-2058f018956f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "252ddd11-30a7-41cd-8eb1-5438543f1525"
        },
        {
            "id": "7b5f3def-6c8a-4a54-a549-c3494b04bbef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "252ddd11-30a7-41cd-8eb1-5438543f1525"
        },
        {
            "id": "f655511a-bfdd-4875-862f-fbbc50c31132",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "252ddd11-30a7-41cd-8eb1-5438543f1525"
        },
        {
            "id": "4c1d3642-5d76-4e63-8b74-237eb0a2c79e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 21,
            "eventtype": 7,
            "m_owner": "252ddd11-30a7-41cd-8eb1-5438543f1525"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "388687ad-7cd0-469d-abe7-14dd3de4fe2f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "ee8a8025-e516-41e7-811b-ee54a367e8c9",
    "visible": true
}