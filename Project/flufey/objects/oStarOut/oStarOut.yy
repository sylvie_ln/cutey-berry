{
    "id": "bce0932e-8408-4669-b8ef-61b5e12536bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStarOut",
    "eventList": [
        {
            "id": "2b641615-b362-4131-8a9f-2a556442ee2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bce0932e-8408-4669-b8ef-61b5e12536bf"
        },
        {
            "id": "798507c0-d7e2-4414-a8a1-0eee782dd577",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "bce0932e-8408-4669-b8ef-61b5e12536bf"
        },
        {
            "id": "f2b51767-a209-47dc-b9bc-5ba8556d0679",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "bce0932e-8408-4669-b8ef-61b5e12536bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad13dca1-2bdd-4f6b-8ff9-734978dc861a",
    "visible": true
}