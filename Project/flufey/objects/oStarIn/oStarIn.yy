{
    "id": "3fcc2b03-0a36-46c5-a835-20faad77022c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oStarIn",
    "eventList": [
        {
            "id": "0ca1c6ca-0ccd-4334-b37c-580b84c9eb96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3fcc2b03-0a36-46c5-a835-20faad77022c"
        },
        {
            "id": "4a5a805f-1706-4c6f-81c6-811ce7d32fd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3fcc2b03-0a36-46c5-a835-20faad77022c"
        },
        {
            "id": "116b5790-7d29-44a7-9d97-8a8b08a99159",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3fcc2b03-0a36-46c5-a835-20faad77022c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bfc3eed5-7f73-4ac1-b495-d17ab46b5192",
    "visible": true
}