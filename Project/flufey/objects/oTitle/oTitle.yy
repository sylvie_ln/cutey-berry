{
    "id": "0c4157c4-2619-4f2a-8c50-4b11abdcf12b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTitle",
    "eventList": [
        {
            "id": "00cb2c8e-f10d-4bf4-a090-6a51c31da913",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0c4157c4-2619-4f2a-8c50-4b11abdcf12b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "508b6b1b-4857-47ca-9623-7944f217340b",
    "visible": true
}