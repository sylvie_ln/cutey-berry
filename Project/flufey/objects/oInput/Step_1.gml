for(var i=0; i<ds_list_size(action_list); i++) {
	var action = action_list[|i];
	if input_held(action) {
		held_time_map[? action]++;	
	} else {
		held_time_map[? action] = 0;	
	}
}