for(var g=0; g<gamepad_get_device_count(); g++) {
	if !gamepad_is_connected(g) {continue;}
	previous_axis_map[? string(g)+":"+string(gp_axislh)] = gamepad_axis_value(g,gp_axislh);
	previous_axis_map[? string(g)+":"+string(gp_axisrh)] = gamepad_axis_value(g,gp_axisrh);
	previous_axis_map[? string(g)+":"+string(gp_axislv)] = gamepad_axis_value(g,gp_axislv);
	previous_axis_map[? string(g)+":"+string(gp_axisrv)] = gamepad_axis_value(g,gp_axisrv);
	if is_undefined(gamepad_hat_count(g)) { continue; }
	for(var h=0; h<gamepad_hat_count(g); h++) {
		previous_hat_map[? string(g)+":"+string(h)] = gamepad_hat_value(g,h);
	}
}
