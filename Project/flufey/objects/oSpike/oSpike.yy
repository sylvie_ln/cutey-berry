{
    "id": "e7adc5cf-12e2-490e-af51-ac03d7231f6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpike",
    "eventList": [
        {
            "id": "b2597b25-3ee9-44bf-b327-7c2dc1d549e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7adc5cf-12e2-490e-af51-ac03d7231f6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1b066efa-ab77-4dfe-9b3b-47dde7a8c3ba",
    "visible": true
}