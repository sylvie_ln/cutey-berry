event_inherited();

cannot_be_damaged = true;
damage = 1;
team = actor_teams.enemy;
hurt_sides = [[1,-1]];
hurt_adjacent = hurt_sides;
depth = global.depth_below;