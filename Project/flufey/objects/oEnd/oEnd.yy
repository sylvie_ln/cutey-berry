{
    "id": "f716589c-2ff9-45b1-a17c-00d51de43607",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnd",
    "eventList": [
        {
            "id": "5ef0c150-ee60-4c8f-88e8-0ca76437438c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f716589c-2ff9-45b1-a17c-00d51de43607"
        },
        {
            "id": "4ee03fe9-edc1-4a45-a6a6-9be15123de75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "f716589c-2ff9-45b1-a17c-00d51de43607"
        },
        {
            "id": "45eed2b1-826f-4b1e-bda9-a4ec1344d401",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f716589c-2ff9-45b1-a17c-00d51de43607"
        },
        {
            "id": "9c32d1d3-4b25-4185-935b-cae066ab4626",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f716589c-2ff9-45b1-a17c-00d51de43607"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "53bf9945-bae9-41a7-9eb6-37ecc098c224",
    "visible": true
}