///@description Follow final kittey
if oChar.final_kittey != noone {
	kittey.target = oChar.final_kittey;
	oChar.final_kittey.targeted_by = kittey;
} else {
	kittey.target = oChar.id;	
	oChar.kittey = kittey;
}
with kittey {
	ds_list_clear(positions);
	event_user(0);
}
oChar.final_kittey = kittey;
kittey.depth = kittey.target.depth+1;