event_inherited();
on_collide = bounce;

can_carry = [true,true];
can_push = [true,true];

hdamp = 0.95;
vdamp = 0.75;
frc = 0.1;

cannot_be_damaged = true;
damage = 1;
team = actor_teams.player;

highlight_color = make_color_hsl_240(irandom(239),239,210);
base_color = make_color_hsl_240(irandom(239),239,230);

mask_index = sKitteySmallMask;
var collision = script_execute(check_collision,x,y);
var unfree = script_execute(handle_collision,collision,true) and collision[0];
if unfree {
	kittey = instance_create_depth(x,y,depth,oKittey);
	kittey.highlight_color = oChar.carry.highlight_color;
	kittey.base_color = oChar.carry.base_color;
	event_user(0);
	instance_destroy();	
	exit;
}