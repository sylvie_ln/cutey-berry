if paused { exit; }

mask_index = sKittey;	
var collision = script_execute(check_collision,x,y);
var unfree = script_execute(handle_collision,collision,true) and collision[0];
if unfree {
	mask_index = sKitteySmallMask;
} else {
	mask_index = sKittey;		
}

image_angle += 90;

var ong = on_ground();

if !ong {
	vv += grav;	
}

if ong {
	hv *= hdamp;
	if abs(hv) < 1 {
		hv = 0;	
	}
}

var vv_before_move = vv;
move(hv,0);
move(vv,1);

if hv == 0 and vv == 0 and vv_before_move >= 0 and on_ground() {
	kittey = instance_create_depth(x,y,depth,oKittey);
	kittey.highlight_color = highlight_color;
	kittey.base_color = base_color;
	instance_destroy();
	exit;
}

if y > room_height {
	kittey = instance_create_depth(x,y,depth,oKittey);
	kittey.highlight_color = highlight_color;
	kittey.base_color = base_color;
	event_user(0);
	instance_destroy();
}