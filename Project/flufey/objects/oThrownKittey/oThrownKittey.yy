{
    "id": "43a3a003-2397-4873-aa63-6fec6c8e235e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oThrownKittey",
    "eventList": [
        {
            "id": "c99d8742-c115-41f0-ae54-76d45a2f7dd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43a3a003-2397-4873-aa63-6fec6c8e235e"
        },
        {
            "id": "62f965e6-e59d-4b86-934d-8fbea7a1bc44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43a3a003-2397-4873-aa63-6fec6c8e235e"
        },
        {
            "id": "ae919661-d898-4028-8347-3ed464a5b363",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "43a3a003-2397-4873-aa63-6fec6c8e235e"
        },
        {
            "id": "00167b78-e28c-4b49-b261-1d9307390a8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "43a3a003-2397-4873-aa63-6fec6c8e235e"
        }
    ],
    "maskSpriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
    "overriddenProperties": null,
    "parentObjectId": "f2033405-ea84-4438-8cd7-9e5a2fe8c237",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
    "visible": true
}