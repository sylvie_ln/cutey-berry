mask_index = sDoorHitbox;
if outside_view() { mask_index = sDoor; exit; }
mask_index = sDoor; 
 
sprite_index = sDoor;
draw_outline_ext(c_black,1)
draw_self();

if level < 0 { exit; }
var iiprev = image_index;
sprite_index = sFruits;
image_index = level;
y -= 16;
draw_outline_ext(c_black,1)
if global.complete[level] {
	draw_self();
}
y += 16;
image_index = iiprev;