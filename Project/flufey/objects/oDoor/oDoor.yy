{
    "id": "035efb5d-6b8a-4b69-91c1-818f30d4f3bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oDoor",
    "eventList": [
        {
            "id": "eb0692f7-03bf-4b22-84aa-fa3d63cda1a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "035efb5d-6b8a-4b69-91c1-818f30d4f3bf"
        },
        {
            "id": "48132629-934f-43b3-bae3-4a0d0e3564f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "035efb5d-6b8a-4b69-91c1-818f30d4f3bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "af38aac6-d55e-4331-bad5-293d64444338",
    "visible": true
}