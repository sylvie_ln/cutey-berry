image_speed = 0;
image_index = 0;
level = door_locations.none;
depth = global.depth_below;

enum door_locations {
	back_to_map = -3,
	none = -1,
	title = 0,
}