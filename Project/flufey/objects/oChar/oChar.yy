{
    "id": "60aaa533-ef22-4435-a04f-28fc60214087",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oChar",
    "eventList": [
        {
            "id": "934069e3-04b3-4d81-9566-2814f554d60f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "96a24bb3-42e8-4fc6-bddd-32c46ab50eeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "0f922dc4-4148-4ba6-99e5-8995206946b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "b0d0428d-bc31-4094-88d6-2e961d7e28e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "bdb02e3e-184f-461e-9d06-03e7d60ea895",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "83086ff6-cf6a-4dbf-b0c2-e575b0c32150",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c8923e94-eaa1-484e-abd4-03d22a110b98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "df5258e5-4693-457f-9a98-95b9e8d21ac6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "0360c2dd-4387-407d-90c6-2c26e946251e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "88295288-e470-4d0b-a707-df50ca209997",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "035efb5d-6b8a-4b69-91c1-818f30d4f3bf",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "86584add-7211-45db-9490-59f5a8446270",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        },
        {
            "id": "e3a52e7e-d3ba-4374-b130-fa37fc73d044",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "60aaa533-ef22-4435-a04f-28fc60214087"
        }
    ],
    "maskSpriteId": "ed7c2103-3541-46ac-9113-03338bcaf4dc",
    "overriddenProperties": null,
    "parentObjectId": "3c65003b-df41-46ef-acd4-f04ae3a9d5b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "27998a6b-fafb-4013-b1f8-f878481884ad",
    "visible": true
}