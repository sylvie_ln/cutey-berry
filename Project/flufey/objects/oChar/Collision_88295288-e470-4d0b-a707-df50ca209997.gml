if (input_pressed("Down") or input_pressed("Up")) and !input_held("Throw") {
	if global.door == -1 {
		visible = false;
		with oActor { paused = true; }
		global.door = other.level;
		if global.door != door_locations.back_to_map {
			global.spawn_x = other.x;
			global.spawn_y = other.y;
		}
		other.image_index = 1;
		event_user(0);
	}
}
