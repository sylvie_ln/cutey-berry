if !global.win {
	visible = false;
	global.win = true;
	with oActor { paused = true; }
	event_user(0);
	with other {
		instance_destroy();	
	}
}
