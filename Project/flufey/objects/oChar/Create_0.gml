event_inherited();

team = actor_teams.player;
hit_time = room_speed;

spd = 3;
acc = spd/10;
frc = acc*2;
spd += frc;
acc += frc;
afrc = 0.15;
jmp = sqrt(2*grav*33);

kittey = noone;
final_kittey = noone;
carry = noone;

throw = false;
throw_yoffset = 12;
throw_angle = 0;
throw_angle_default = 0;
throw_angle_max = 75;
throw_dir = 0;
throw_adjust_spd = 5;
throw_min_spd = 2;
throw_spd = throw_min_spd;
throw_max_spd = 6;

pushable = [true,true];
carryable = [true,true];
can_push = [false,true]
can_carry = [true,true];

global.win = false;
global.door = door_locations.none;

depth = global.depth_middle