if global.win { 
	if !instance_exists(oStarOut) {
		global.complete[global.level] = true;
		global.level = 0;
		var all_clear = true;
		for(var i=0; i<global.total_levels; i++) {
			if !global.complete[i] {
				all_clear = false;	
			}
		}
		if !all_clear {
			room_goto(rmMap);
		} else {
			room_goto(rmEnding);	
		}
	}
	save_game();
	exit; 
}
if global.door != -1 {
	if !instance_exists(oStarOut) {
		if global.door == door_locations.back_to_map {
			var rm = rmMap;
		} else if global.door == door_locations.title {
			var rm = rmTitle;	
		} else {
			var rm = asset_get_index("rm"+string(global.door));
		}
		if room_exists(rm) {
			global.level = global.door;
			room_goto(rm);	
		} else {
			global.door = -1;	
			visible = true;
		}
	}
	save_game();
	exit; 
}

var ong = on_ground();

var left = input_held("Left")
var right = input_held("Right")
var hdir = right-left;
if left and right {
	hdir = (input_held_time("Left") < input_held_time("Right")) ? -1 : 1;	
}

if !knockback {
	if abs(hv+acc*hdir) <= spd {
		hv += acc*hdir;
	} else {
		if hdir != sign(hv) {
			hv += acc*hdir;	
		} else {
			if hdir > 0 {
				hv = max(hv,spd);	
			} 
			if hdir < 0 {
				hv = min(hv,-spd);
			}
		}
	}

	if hv != 0 {
		var s = sign(hv);
		if ong {
			hv -= frc*sign(hv);
		} else {
			hv -= afrc*sign(hv);
		}
		if sign(hv) != s {
			hv = 0;	
		}
	}

	if !ong {
		vv += grav;
	}

	var jumped = false;
	if ong and input_pressed("Jump") {
		vv = -jmp;
		jumped = true;
		sfx_play(sfxJump,0.2);
	}
	if input_released("Jump") and vv < 0 {
		vv /= 3;
	}
	
	var vv_pre_move = vv; 
	
	move(hv,0);
	move(vv,1);
	
	if (hdir != 0 and ong and !on_ground() and vv_pre_move >= 0) or jumped {
		disembark = true;
	}
}

if kittey != noone and instance_exists(kittey) {
	if input_pressed("Throw") {
		sfx_play(sfxThrowCharge);
		throw_angle = throw_angle_default;	
		throw_spd = throw_min_spd;
		// the front kitty becomes our carried kitty
		with kittey {
			var k = instance_create_depth(x,y,depth,oCarryKittey);
			k.highlight_color = highlight_color;
			k.base_color = base_color;
		}
		carry = k;
		// our new front kitty is the follower of the old front kitty (if any)
		var old_kittey = kittey;
		kittey = old_kittey.targeted_by;
		with old_kittey {
			instance_destroy();	
		}
		if kittey != noone {
			// the new front kitty now follows the player
			kittey.target = id;
			with kittey {
				ds_list_clear(positions);
				event_user(0);
			}
		} else {
			// if there was no follower, we have no final kitty anymore
			final_kittey = noone;
		}
		
	}
}

if carry != noone and instance_exists(carry) {
	if input_held("Throw") {
		throw_spd += (throw_max_spd-throw_spd) * 0.1;
		if abs(throw_max_spd-throw_spd) < 0.125 {
			throw_spd = throw_max_spd;	
		}
		var up = input_held("Up");
		var down = input_held("Down");
		var vdir = down-up;
		if up and down {
			vdir = (input_held_time("Up") < input_held_time("Down")) ? -1 : 1;	
		}
		throw_angle -= throw_adjust_spd*vdir;
		throw_angle = clamp(throw_angle,-throw_angle_max,throw_angle_max);
		if image_xscale == 1 {
			throw_dir = throw_angle;	
		} else {
			throw_dir = 180-throw_angle;	
		}
		while throw_dir < 0 {
			throw_dir += 360;	
		}
		while throw_dir >= 360 {
			throw_dir -= 360;
		}	
	}

	if input_released("Throw") or !input_held("Throw") {
		sfx_play(sfxThrow);
		var vfactor = 1.5;
		var thrown_kittey = instance_create_depth(x,y-16,depth-1,oThrownKittey);
		if instance_exists(thrown_kittey) {
			thrown_kittey.hv = lengthdir_x(throw_spd,throw_dir);
			thrown_kittey.vv = lengthdir_y(throw_spd,throw_dir)*vfactor;
			thrown_kittey.highlight_color = carry.highlight_color;
			thrown_kittey.base_color = carry.base_color;
		}
		with carry {
			instance_destroy();	
		}
		carry = noone;
		throw = true;
		image_index = 0;
	}
}

with instance_place(x,y,oKittey) {
	if target == noone {
		sfx_play(sfxKitteyCollect);
		target = other.id; // the kitty we just collected targets the player
		depth = other.depth+1;
		if other.kittey != noone { // if we already have a kitty following us
			other.kittey.target = id; // this kitty now targets the new kitty
			other.kittey.depth = target.depth+1;
			targeted_by = other.kittey; // the new kitty is targeted by the following kitty
			with other.kittey {
				ds_list_clear(positions);
				event_user(0);
			}
		} else {
			other.final_kittey = id; // if no kitty is following us, the new kitty is the final one	
		}
		other.kittey = id; // the front kitty is updated to the just-collected kitty
		event_user(0);
	}
}

if hdir != 0 {
	image_xscale = hdir;	
}

var sprite = "sChar";

if knockback {
	sprite += "Hurt";
} else if throw {
	sprite += "Throw"	
} else {
	if carry != noone and instance_exists(carry) {
		sprite += "Carry";	
	}
	if !ong {
		if vv > 0 {
			sprite += "Fall";
		} else {
			sprite += "Jump";
		}
	} else {
		if hdir != 0 or sprite_index == sCharWalk or sprite_index == sCharCarryWalk {
			if sprite_index == sCharWalk or sprite_index == sCharCarryWalk {
				if hv == 0 and floor(image_index) mod 2 == 0 {
					sprite += "Stand";	
				} else {
					sprite += "Walk";	
				}
			} else {
				sprite += "Walk";	
			}
		} else {
			sprite += "Stand";
		}
	}
}

sprite = asset_get_index(sprite);
if sprite_exists(sprite) {
	sprite_index = sprite;	
}