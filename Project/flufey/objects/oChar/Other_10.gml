///@description Star Out
sfx_play(sfxStarOut);
var stars = 12;
for(var i=0; i<stars; i++) {
	var star = instance_create_depth(other.x,other.y,depth,oStarOut);
	star.speed = 3;
	star.direction = (360/stars)*i;
	with star { event_user(0); }
}