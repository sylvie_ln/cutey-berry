{
    "id": "505f9368-2931-4d81-8d8c-262e9f133781",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCarryKittey",
    "eventList": [
        {
            "id": "87edf6ea-785a-45ff-98c1-7d4045f1e72b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "505f9368-2931-4d81-8d8c-262e9f133781"
        },
        {
            "id": "591722ed-368a-4391-b7b1-479abc4d8348",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "505f9368-2931-4d81-8d8c-262e9f133781"
        },
        {
            "id": "7ebdaba5-e45f-4920-9cef-2be692638f66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "505f9368-2931-4d81-8d8c-262e9f133781"
        }
    ],
    "maskSpriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4d6479f8-b620-4482-9770-ace6d9339d05",
    "visible": true
}