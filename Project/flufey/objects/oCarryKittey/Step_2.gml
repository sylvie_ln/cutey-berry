var xp = oChar.x;
var yp = oChar.y-oChar.throw_yoffset;
image_xscale = oChar.image_xscale;
if point_distance(x,y,xp,yp) < 2 or lock {
	x = xp;
	y = yp;
	lock = true;
} else {
	x += (xp-x)*0.5;	
	y += (yp-y)*0.5;
}