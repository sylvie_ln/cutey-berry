target = amplitude*dir*sin(2*pi*stepc/period);
if axis == 0  or axis == 2 {
	hv = target - current;
	move(hv,0);
} if axis == 1 or axis == 2 {
	vv = target - current;
	move(vv,1);
}
current = target;
stepc++;