event_inherited();

axis = 1;
amplitude = 32;
period = room_speed*2;
stepc = 0;
current = 0;
target = 0;
dir = 1;

can_push = [true,true];
can_carry = [true,true];

cannot_be_damaged = true;