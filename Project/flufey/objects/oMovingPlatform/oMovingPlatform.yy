{
    "id": "1040753a-927f-40d9-9efd-972fb55baf73",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oMovingPlatform",
    "eventList": [
        {
            "id": "cf24e4ed-a57d-4674-9613-dbf6c960c661",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1040753a-927f-40d9-9efd-972fb55baf73"
        },
        {
            "id": "11e76963-a8a8-4e1f-9e4a-9796eb309cbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1040753a-927f-40d9-9efd-972fb55baf73"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f2033405-ea84-4438-8cd7-9e5a2fe8c237",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9bc4060d-895a-4c79-8c5f-d3f15edf0f69",
    "visible": true
}