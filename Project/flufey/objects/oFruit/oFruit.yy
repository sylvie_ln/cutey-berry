{
    "id": "c8923e94-eaa1-484e-abd4-03d22a110b98",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFruit",
    "eventList": [
        {
            "id": "7d3edc98-5270-4f5c-8043-41a2da357f5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8923e94-eaa1-484e-abd4-03d22a110b98"
        },
        {
            "id": "34d2717b-799b-4013-b9c7-01a2f09c41b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c8923e94-eaa1-484e-abd4-03d22a110b98"
        },
        {
            "id": "24f51763-850e-4dc1-90d1-8f303dd944c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c8923e94-eaa1-484e-abd4-03d22a110b98"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a60f7dc2-02ec-4cef-8a3e-2b1db5dd29a2",
    "visible": true
}