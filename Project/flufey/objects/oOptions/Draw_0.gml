draw_set_alpha(0.75);
draw_set_color(c_black);
draw_rectangle(0,0,room_width,room_height,false);
draw_set_color(c_white);
draw_set_alpha(1);
var yp = 8;
for(var i=0; i<options.last; i++) {
	draw_set_font(global.sylvie_font_bold_2x);
	var xp = (room_width div 2)-(string_width("Let's Go Play!") div 2)
	switch(i) {
		case options.back:
		draw_set_font(global.sylvie_font_bold_2x);
		yp += 4;
		skip = 32;
		break;
		case options.key_config:
		yp += 4;
		case options.scaling:
		case options.volume:
		skip = 14;
		draw_set_font(global.sylvie_font_bold);
		break;
		default:
		skip = 14;
		draw_set_font(global.sylvie_font_bold);
	}
	if i == selected {
		draw_sprite_ext(sChar,0,xp-14+1,yp+(skip div 3),1,1,0,c_black,1);
		draw_sprite_ext(sChar,0,xp-14-1,yp+(skip div 3),1,1,0,c_black,1);
		draw_sprite_ext(sChar,0,xp-14,yp+(skip div 3)+1,1,1,0,c_black,1);
		draw_sprite_ext(sChar,0,xp-14,yp+(skip div 3)-1,1,1,0,c_black,1);
		draw_sprite(sChar,0,xp-14,yp+(skip div 3));
	}
	draw_text(xp,yp,words[i]);
	var control_xp = xp+40;
	var other_xp = xp+80;
	xp += string_width(words[i])+16;
	switch(i) {
		case options.back:
		break;
		case options.key_config:
		break;
		case options.scaling:
			xp = other_xp;
			draw_text(xp,yp,"< "+string(global.scale)+"x >");
		break;
		case options.volume:
			xp = other_xp;
			draw_text(xp,yp,"< "+string(round(global.volume*100))+"% >");
		break;
		default:
			var xp = control_xp;
			var list = oInput.actions[? act[i]];
			for(var j=0; j<ds_list_size(list); j++) {
				var istr = input_string_sprite(list[|j]);
				var inp = list[|j];
				if inp[|0] == input_kind.waiting {
					draw_set_alpha(0.5);
				} 
				//show_debug_message(istr);
				draw_text_sprite(xp,yp,istr);
				draw_set_alpha(1);
				if selected_key[i] == j  and selected == i {
					draw_rectangle(xp-2,yp-1,
					xp+string_width_sprite(istr)+1,
					yp+string_height(istr)+1,true);
				}
				xp += string_width_sprite(istr);
				if j < ds_list_size(list)-1 {
					istr = " or ";	
					draw_text(xp,yp,istr);
					xp += string_width(istr);
				}
			}
	}
	yp += skip;
}
draw_set_font(global.sylvie_font_bold);
var inp = oInput.actions[? "Jump"];
var k1 = inp[|0];
var k2 = inp[|1];
var inp = oInput.actions[? "Throw"];
var k3 = inp[|0];
var k4 = inp[|1];
var inp = oInput.actions[? "Left"];
var l = inp[|0];
var inp = oInput.actions[? "Right"];
var r = inp[|0];
switch(selected) {
	case options.back:
		var t = "Press "+input_string_sprite(k1)+"/"+input_string_sprite(k3)+"/"+input_string_sprite(k2)+"/"+input_string_sprite(k4)+" to play!"
	break;
	case options.key_config:
		var t = "You are simpley a JIGGLER."
	break;
	case options.scaling:
		var t = "Use "+input_string_sprite(l)+" and "+input_string_sprite(r)+" to configure the scaling factor."
	break;
	case options.volume:
		var t = "Use "+input_string_sprite(l)+" and "+input_string_sprite(r)+" to change sound volume."
	break;
	default:
		if waiting {
			var t = "Press the button you want to assign to this action."	
		} else {
			var t = "Press "+input_string_sprite(k1)+"/"+input_string_sprite(k3)+"/"+input_string_sprite(k2)+"/"+input_string_sprite(k4)+" to configure input."
		}
}
draw_text_sprite((room_width div 2)-(string_width_sprite(t) div 2),room_height-16,t);