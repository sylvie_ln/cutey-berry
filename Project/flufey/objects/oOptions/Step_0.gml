if waiting {
	var key = input_any_key();
	if key != -1 {
		input_assign_key(act[selected],key);
		waiting = false;
		exit;
	}
	var button = input_any_gamepad_button();
	if button != -1 {
		input_assign_gamepad_button(act[selected],button);
		waiting = false;
		exit;
	}
	var axis_dir = input_any_gamepad_axis();
	var axis = axis_dir[0];
	var dir = axis_dir[1];
	if axis != -1 {
		input_assign_gamepad_axis(act[selected],axis,dir);
		waiting = false;
		exit;
	}
	var hat_dir = input_any_gamepad_hat();
	var hat = hat_dir[0];
	var dir = hat_dir[1];
	if dir != 0 {
		input_assign_gamepad_hat(act[selected],hat,dir);
		waiting = false;
		exit;
	}
	exit;	
}

if input_pressed("Jump") or input_pressed("Throw") {
	sfx_play(sfxJump,0.2);
	switch(selected) {
		case options.back: save_options(); room_goto(rmTitle); break;
		case options.key_config:
		break;
		case options.scaling:
		break;
		case options.volume:
		break;
		default:
		if !waiting {
			input_assign_wait(act[selected],selected_key[selected]);
			waiting = true;
			exit;
		}
	}
}

if input_pressed("Left") {
	sfx_play(sfxJump,0.2);
	switch(selected) {
		case options.back:
		case options.key_config:
		break;
		case options.scaling:
			global.scale--;
			if global.scale < 1 {
				global.scale = 1;	
			}
			window_set_scale(global.scale);
			alarm[0] = 1;
		break;
		case options.volume:
			global.volume -= 0.1;
			global.volume = round(global.volume*10)/10;
			if global.volume < 0 {
				global.volume = 0;	
			}
			audio_set_master_gain(0,global.volume);
		break;
		default:
		selected_key[selected] = 1-selected_key[selected];
	}
}
if input_pressed("Right") {
	sfx_play(sfxJump,0.2);
	switch(selected) {
		case options.back:
		case options.key_config:
		break;
		case options.scaling:
			global.scale++;
			if global.scale > global.max_scale {
				global.scale = global.max_scale;	
			}
			window_set_scale(global.scale);
			alarm[0] = 1;
		break;
		case options.volume:
			global.volume += 0.1;
			global.volume = round(global.volume*10)/10;
			if global.volume > 1 {
				global.volume = 1;	
			}
			audio_set_master_gain(0,global.volume);
		break;
		default:
		selected_key[selected] = 1-selected_key[selected];
	}
}

if input_pressed("Up") {
	sfx_play(sfxJump,0.2);
	var prev_selected = selected;
	selected--;
	if selected == options.key_config {
		selected--;	
	}
	if selected < 0 {
		selected = options.last-1;
	}
	selected_key[selected] = selected_key[prev_selected];
}

if input_pressed("Down") {
	sfx_play(sfxJump,0.2);
		var prev_selected = selected;
	selected++;
	if selected == options.key_config {
		selected++;	
	}
	if selected >= options.last {
		selected = 0
	}
		selected_key[selected] = selected_key[prev_selected];
}