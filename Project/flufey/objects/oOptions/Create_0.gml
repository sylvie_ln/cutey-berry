enum options {
	scaling,
	volume,
	key_config,
	up_key,
	down_key,
	left_key,
	right_key,
	jump_key,
	interact_key,
	back,
	last
}

words[options.scaling] = "Screen Scaling"
words[options.volume] = "Sound Volume"
words[options.key_config] = "Button Configuration"
words[options.up_key] = "Up"
words[options.down_key] = "Down"
words[options.left_key] = "Left"
words[options.right_key] = "Right"
words[options.jump_key] = "Jump"
words[options.interact_key] = "Throw"
words[options.back] = "Let's Go Play!"
selected = 0;


act[options.up_key] = "Up"
act[options.down_key] = "Down"
act[options.left_key] = "Left"
act[options.right_key] = "Right"
act[options.jump_key] = "Jump"
act[options.interact_key] = "Throw"

for(var i=0; i<options.last; i++) {
	selected_key[i] = 0;	
}
waiting = false;

compute_max_scale();