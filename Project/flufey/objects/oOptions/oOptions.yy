{
    "id": "5c9a9500-edac-4f9d-9d1e-9a7ce39c75bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oOptions",
    "eventList": [
        {
            "id": "375e2f1e-e372-4721-863d-107f30654e5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c9a9500-edac-4f9d-9d1e-9a7ce39c75bb"
        },
        {
            "id": "96fbfcfe-3ccf-4b41-a92b-681d34e7e44d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5c9a9500-edac-4f9d-9d1e-9a7ce39c75bb"
        },
        {
            "id": "0dcfa555-272d-417b-bee0-a60dcebfd657",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5c9a9500-edac-4f9d-9d1e-9a7ce39c75bb"
        },
        {
            "id": "68141d17-3be9-4ea4-8361-770b255901f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5c9a9500-edac-4f9d-9d1e-9a7ce39c75bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}